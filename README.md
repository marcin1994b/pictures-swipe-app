This app is not available on Google Play Store yet. I need more time.

This is amazing app to watching lovely pictures from unsplash.com service.
Thanks to stack view you can easily watching pictures and deciding which you like and don't like.
After that, you can see all picuters you liked and show them your friend by sharing or sending them.
In Addition, you can see details about the picture, picture's author and download the picture.
You can set how many picuters you want to have on stack, when the app will download next and quality of them.

To develop it, I use kotlin, Retrofit, RxJava, RxAndroid, GSON, Picasso, RxBinding, JUnit, Mockito and Robolectric, Firebase Tools and a lot o constraints layouts! ;)

PS. The app is ugly yet, but I hope, code is really nice. So see the code!