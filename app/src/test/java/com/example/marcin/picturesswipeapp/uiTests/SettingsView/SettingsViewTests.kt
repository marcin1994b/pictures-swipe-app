package com.example.marcin.picturesswipeapp.uiTests.SettingsView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.SettingsView.SettingsFragment
import com.example.marcin.picturesswipeapp.ui.SettingsView.SettingsViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertNotNull
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.settingscardview.view.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class SettingsViewTests {

    private lateinit var view: SettingsFragment
    @Mock private lateinit var presenter: SettingsViewContracts.SettingsViewPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        view = SettingsFragment()
        view.presenter = presenter
        startFragment(view)
    }

    @Test
    @Throws(Exception::class)
    fun testViewIsNotNull(){
        assertNotNull(view)
    }

    @Test
    fun testSetStackImagesQualityCardViewMethodForFirstChecked(){
        view.setStackImagesQualityCardView(1)
        assert(view.stack_img_resolution.radioButton1.isChecked)
        assert(!view.stack_img_resolution.radioButton2.isChecked)
        assert(!view.stack_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetStackImagesQualityCardViewMethodForSecondChecked(){
        view.setStackImagesQualityCardView(2)
        assert(!view.stack_img_resolution.radioButton1.isChecked)
        assert(view.stack_img_resolution.radioButton2.isChecked)
        assert(!view.stack_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetStackImagesQualityCardViewMethodForThirdChecked(){
        view.setStackImagesQualityCardView(3)
        assert(!view.stack_img_resolution.radioButton1.isChecked)
        assert(!view.stack_img_resolution.radioButton2.isChecked)
        assert(view.stack_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetFullImageQualityCardViewMethodWhenFirstIsChecked(){
        view.setFullImageQualityCardView(1)
        assert(view.full_img_resolution.radioButton1.isChecked)
        assert(!view.full_img_resolution.radioButton2.isChecked)
        assert(!view.full_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetFullImageQualityCardViewMethodWhenSecondIsChecked(){
        view.setFullImageQualityCardView(2)
        assert(!view.full_img_resolution.radioButton1.isChecked)
        assert(view.full_img_resolution.radioButton2.isChecked)
        assert(!view.full_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetFullImageQualityCardViewMethodWhenThirdIsChecked(){
        view.setFullImageQualityCardView(3)
        assert(!view.full_img_resolution.radioButton1.isChecked)
        assert(!view.full_img_resolution.radioButton2.isChecked)
        assert(view.full_img_resolution.radioButton3.isChecked)
    }

    @Test
    fun testSetMaxSizeOfStackCardViewMethodWhenFirstIsChecked(){
        view.setMaxSizeOfStackCardView(1)
        assert(view.max_stack_size.radioButton1.isChecked)
        assert(!view.max_stack_size.radioButton2.isChecked)
        assert(!view.max_stack_size.radioButton3.isChecked)
    }

    @Test
    fun testSetMaxSizeOfStackCardViewMethodWhenSecondIsChecked(){
        view.setMaxSizeOfStackCardView(2)
        assert(!view.max_stack_size.radioButton1.isChecked)
        assert(view.max_stack_size.radioButton2.isChecked)
        assert(!view.max_stack_size.radioButton3.isChecked)
    }

    @Test
    fun testSetMaxSizeOfStackCardViewMethodWhenThirsIsChecked(){
        view.setMaxSizeOfStackCardView(3)
        assert(!view.max_stack_size.radioButton1.isChecked)
        assert(!view.max_stack_size.radioButton2.isChecked)
        assert(view.max_stack_size.radioButton3.isChecked)
    }

    @Test
    fun testSetMinSizeOfStackCardViewMethodWhenFirstIsChecked(){
        view.setMinSizeOfStackCardView(1)
        assert(view.min_stack_size.radioButton1.isChecked)
        assert(!view.min_stack_size.radioButton2.isChecked)
        assert(!view.min_stack_size.radioButton3.isChecked)
    }

    @Test
    fun testSetMinSizeOfStackCardViewMethodWhenSecondIsChecked(){
        view.setMinSizeOfStackCardView(2)
        assert(!view.min_stack_size.radioButton1.isChecked)
        assert(view.min_stack_size.radioButton2.isChecked)
        assert(!view.min_stack_size.radioButton3.isChecked)
    }

    @Test
    fun testSetMinSizeOfStackCardViewMethodWhenThirsIsChecked(){
        view.setMinSizeOfStackCardView(3)
        assert(!view.min_stack_size.radioButton1.isChecked)
        assert(!view.min_stack_size.radioButton2.isChecked)
        assert(view.min_stack_size.radioButton3.isChecked)
    }

}