package com.example.marcin.picturesswipeapp.modelTests

import android.content.Context
import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class SharedPreferencesTests{

    lateinit var context: Context
    lateinit var sharedPreferencesProvider: SharedPreferencesProvider

    @Before
    fun setUp(){
        context = RuntimeEnvironment.application
        sharedPreferencesProvider = SharedPreferencesProvider(context)
    }

    @Test
    fun saveAndRestoreDataTest(){
        val key = sharedPreferencesProvider.MIN_STACK_SIZE_CONFIG_NAME
        val value = 123
        sharedPreferencesProvider.saveData(key, value)
        assertEquals(value, sharedPreferencesProvider.restoreData(key))
    }
}
