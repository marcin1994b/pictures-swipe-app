package com.example.marcin.picturesswipeapp.uiTests.AccountView

import android.view.View
import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.AccountView.AccountFragment
import com.example.marcin.picturesswipeapp.ui.AccountView.AccountViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertEquals
import kotlinx.android.synthetic.main.change_email_layout.view.*
import kotlinx.android.synthetic.main.change_password_layout.view.*
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.restart_account_layout.view.*
import kotlinx.android.synthetic.main.verify_email_layout.view.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class AccountViewTests {

    private lateinit var view: AccountFragment
    @Mock private lateinit var presenter: AccountViewContracts.AccountViewPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        view = AccountFragment()
        view.presenter = presenter
        startFragment(view)
    }

    @Test
    @Throws(Exception::class)
    fun testViewIsNotNull(){
        Assert.assertNotNull(view)
    }

    @Test
    @Throws(Exception::class)
    fun testExpandVerifyViewMethod(){
        view.expandVerifyView()
        assertEquals(View.VISIBLE, view.verifyEmailView.noteTextView.visibility)
        assertEquals(View.VISIBLE, view.verifyEmailView.verifyButton.visibility)
        assertEquals(View.INVISIBLE, view.verifyEmailView.verifyEmailProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testExpandChangeEmailViewMethod(){
        view.expandChangeEmailView()
        assertEquals(View.VISIBLE, view.changeEmailView.saveEmailButton.visibility)
        assertEquals(View.VISIBLE, view.changeEmailView.passwordInputWrapper.visibility)
        assertEquals(View.VISIBLE, view.changeEmailView.newEmailInputWrapper.visibility)
        assertEquals(View.INVISIBLE, view.changeEmailView.changeEmailProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testExpandChangePasswordViewMethod(){
        view.expandChangePasswordView()
        assertEquals(View.VISIBLE, view.changePasswordView.currentPasswordWrapper.visibility)
        assertEquals(View.VISIBLE, view.changePasswordView.firstNewPasswordWrapper.visibility)
        assertEquals(View.VISIBLE, view.changePasswordView.secondNewPasswordWrapper.visibility)
        assertEquals(View.VISIBLE, view.changePasswordView.savePasswordChangeButton.visibility)
        assertEquals(View.INVISIBLE, view.changePasswordView.changePasswordProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testExpandRestartAccountViewMethod(){
        view.expandRestartAccountView()
        assertEquals(View.VISIBLE, view.restartAccountView.messageTextView.visibility)
        assertEquals(View.VISIBLE, view.restartAccountView.passwordWrapper.visibility)
        assertEquals(View.VISIBLE, view.restartAccountView.submitButton.visibility)
        assertEquals(View.INVISIBLE, view.restartAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testExpandRemoveAccountViewMethod(){
        view.expandRestartAccountView()
        assertEquals(View.VISIBLE, view.removeAccountView.messageTextView.visibility)
        assertEquals(View.VISIBLE, view.removeAccountView.passwordWrapper.visibility)
        assertEquals(View.VISIBLE, view.removeAccountView.submitButton.visibility)
        assertEquals(View.INVISIBLE, view.removeAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testCollapseVerifyViewMethod(){
        view.collapseVerifyView()
        assertEquals(View.GONE, view.verifyEmailView.noteTextView.visibility)
        assertEquals(View.GONE, view.verifyEmailView.verifyButton.visibility)
        assertEquals(View.GONE, view.verifyEmailView.verifyEmailProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testCollapseChangeEmailViewMethod(){
        view.collapseChangeEmailView()
        assertEquals(View.GONE, view.changeEmailView.saveEmailButton.visibility)
        assertEquals(View.GONE, view.changeEmailView.passwordInputWrapper.visibility)
        assertEquals(View.GONE, view.changeEmailView.newEmailInputWrapper.visibility)
        assertEquals(View.GONE, view.changeEmailView.changeEmailProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testCollapseChangePasswordViewMethod(){
        view.collapseChangePasswordView()
        assertEquals(View.GONE, view.changePasswordView.currentPasswordWrapper.visibility)
        assertEquals(View.GONE, view.changePasswordView.firstNewPasswordWrapper.visibility)
        assertEquals(View.GONE, view.changePasswordView.secondNewPasswordWrapper.visibility)
        assertEquals(View.GONE, view.changePasswordView.savePasswordChangeButton.visibility)
        assertEquals(View.GONE, view.changePasswordView.changePasswordProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testCollapseRestartAccountViewMethod(){
        view.collapseRestartAccountView()
        assertEquals(View.GONE, view.restartAccountView.messageTextView.visibility)
        assertEquals(View.GONE, view.restartAccountView.passwordWrapper.visibility)
        assertEquals(View.GONE, view.restartAccountView.submitButton.visibility)
        assertEquals(View.GONE, view.restartAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    @Throws(Exception::class)
    fun testCollapseRemoveAccountViewMethod(){
        view.collapseRemoveAccountView()
        assertEquals(View.GONE, view.removeAccountView.messageTextView.visibility)
        assertEquals(View.GONE, view.removeAccountView.passwordWrapper.visibility)
        assertEquals(View.GONE, view.removeAccountView.submitButton.visibility)
        assertEquals(View.GONE, view.removeAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    fun testSetEnabledVerifyEmailButtonMethod(){
        view.setEnabledVerifyEmailButton(true)
        assertEquals(true, view.verifyEmailView.verifyButton.isEnabled)
        view.setEnabledVerifyEmailButton(false)
        assertEquals(false, view.verifyEmailView.verifyButton.isEnabled)
    }

    @Test
    fun testSetEnabledChangeEmailButtonMethod(){
        view.setEnabledChangeEmailButton(true)
        assertEquals(true, view.changeEmailView.saveEmailButton.isEnabled)
        view.setEnabledChangeEmailButton(false)
        assertEquals(false, view.changeEmailView.saveEmailButton.isEnabled)
    }

    @Test
    fun testSetEnabledChangePasswordButtonMethod(){
        view.setEnabledChangePasswordButton(true)
        assertEquals(true, view.changePasswordView.savePasswordChangeButton.isEnabled)
        view.setEnabledChangePasswordButton(false)
        assertEquals(false, view.changePasswordView.savePasswordChangeButton.isEnabled)
    }

    @Test
    fun testSetEnabledRestartAccountButtonMethod(){
        view.setEnabledRestartAccountButton(true)
        assertEquals(true, view.restartAccountView.submitButton.isEnabled)
        view.setEnabledRestartAccountButton(false)
        assertEquals(false, view.restartAccountView.submitButton.isEnabled)
    }

    @Test
    fun testSetEnabledRemoveAccountButtonMethod(){
        view.setEnabledRemoveAccountButton(true)
        assertEquals(true, view.removeAccountView.submitButton.isEnabled)
        view.setEnabledRemoveAccountButton(false)
        assertEquals(false, view.removeAccountView.submitButton.isEnabled)
    }

    @Test
    fun testSetVisibilityVerifyButtonMethod(){
        view.setVisibilityVerifyButton(true)
        assertEquals(View.VISIBLE, view.verifyEmailView.verifyButton.visibility)
        view.setVisibilityVerifyButton(false)
        assertEquals(View.INVISIBLE, view.verifyEmailView.verifyButton.visibility)
    }

    @Test
    fun testSetVisibilityChangeEmailButtonMethod(){
        view.setVisibilityChangeEmailButton(true)
        assertEquals(View.VISIBLE, view.changeEmailView.saveEmailButton.visibility)
        view.setVisibilityChangeEmailButton(false)
        assertEquals(View.INVISIBLE, view.changeEmailView.saveEmailButton.visibility)
    }

    @Test
    fun testSetVisibilityChangePasswordButtonMethod(){
        view.setVisibilityChangePasswordButton(true)
        assertEquals(View.VISIBLE, view.changePasswordView.savePasswordChangeButton.visibility)
        view.setVisibilityChangePasswordButton(false)
        assertEquals(View.INVISIBLE, view.changePasswordView.savePasswordChangeButton.visibility)
    }

    @Test
    fun testSetVisibilityRestartAccountButtonMethod(){
        view.setVisibilityRestartAccountButton(true)
        assertEquals(View.VISIBLE, view.restartAccountView.submitButton.visibility)
        view.setVisibilityRestartAccountButton(false)
        assertEquals(View.INVISIBLE, view.restartAccountView.submitButton.visibility)
    }

    @Test
    fun testSetVisibilityRemoveAccountButtonMethod(){
        view.setVisibilityRemoveAccountButton(true)
        assertEquals(View.VISIBLE, view.removeAccountView.submitButton.visibility)
        view.setVisibilityRemoveAccountButton(false)
        assertEquals(View.INVISIBLE, view.removeAccountView.submitButton.visibility)
    }

    @Test
    fun testSetVisibilityVerifyProgressBarMethod(){
        view.setVisibilityVerifyProgressBar(true)
        assertEquals(View.VISIBLE, view.verifyEmailView.verifyEmailProgressBar.visibility)
        view.setVisibilityVerifyProgressBar(false)
        assertEquals(View.INVISIBLE, view.verifyEmailView.verifyEmailProgressBar.visibility)
    }

    @Test
    fun testSetVisibilityChangeEmailProgressBarMethod(){
        view.setVisibilityChangeEmailProgressBar(true)
        assertEquals(View.VISIBLE, view.changeEmailView.changeEmailProgressBar.visibility)
        view.setVisibilityChangeEmailProgressBar(false)
        assertEquals(View.INVISIBLE, view.changeEmailView.changeEmailProgressBar.visibility)
    }

    @Test
    fun testSetVisibilityChangePasswordProgressBarMethod(){
        view.setVisibilityChangePasswordProgressBar(true)
        assertEquals(View.VISIBLE, view.changePasswordView.changePasswordProgressBar.visibility)
        view.setVisibilityChangePasswordProgressBar(false)
        assertEquals(View.INVISIBLE, view.changePasswordView.changePasswordProgressBar.visibility)
    }

    @Test
    fun testSetVisibilityRestartAccountProgressBarMethod(){
        view.setVisibilityRestartAccountProgressBar(true)
        assertEquals(View.VISIBLE, view.restartAccountView.restartAccountProgressBar.visibility)
        view.setVisibilityRestartAccountProgressBar(false)
        assertEquals(View.INVISIBLE, view.restartAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    fun testSetVisibilityRemoveAccountProgressBarMethod(){
        view.setVisibilityRemoveAccountProgressBar(true)
        assertEquals(View.VISIBLE, view.removeAccountView.restartAccountProgressBar.visibility)
        view.setVisibilityRemoveAccountProgressBar(false)
        assertEquals(View.INVISIBLE, view.removeAccountView.restartAccountProgressBar.visibility)
    }

    @Test
    fun testSetNewEmailWrapperErrorInChangeEmailViewMethod(){
        val testString = "example example 123"
        view.setNewEmailWrapperErrorInChangeEmailView(testString)
        assertEquals(testString, view.changeEmailView.newEmailInputWrapper.error.toString())
    }

    @Test
    fun testSetFirstNewPassWrapperErrorInChangePasswordView(){
        val testString = "example example 123"
        view.setFirstNewPassWrapperErrorInChangePasswordView(testString)
        assertEquals(testString, view.changePasswordView.firstNewPasswordWrapper.error.toString())
    }

    @Test
    fun testSetSecondNewPassWrapperErrorInChangePasswordViewMethod(){
        val testString = "example example 123"
        view.setSecondNewPassWrapperErrorInChangePasswordView(testString)
        assertEquals(testString, view.changePasswordView.secondNewPasswordWrapper.error.toString())
    }
}