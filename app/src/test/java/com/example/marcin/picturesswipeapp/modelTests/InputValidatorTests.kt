package com.example.marcin.picturesswipeapp.modelTests

import com.example.marcin.picturesswipeapp.Model.InputValidator
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class InputValidatorTests {

    lateinit var inputValidator: InputValidator

    @Before
    fun init(){
        inputValidator = InputValidator()
    }

    @Test
    fun emailValidationForEmptyString() {
        val emptyString = ""
        assertEquals(Strings.EMAIL_NOT_CONTAINS, inputValidator.emailValidation(emptyString))
    }

    @Test
    fun emailValidationForStringOnlyMonkey(){
        val stringWithMonkey = "@"
        assertEquals(Strings.EMAIL_SEQUENCE_BEFORE_TOO_SHORT, inputValidator.emailValidation(stringWithMonkey))
    }

    @Test
    fun emailValidationForStringWithMonkeyWhitoutDot(){
        val stringWithMonkey = "example@"
        assertEquals(Strings.EMAIL_SEQUENCE_BEHIND_TOO_SHORT, inputValidator.emailValidation(stringWithMonkey))
    }

    @Test
    fun emailValidationForStringWithMonkeyAndDot(){
        val stringWithMonkey = "example@."
        assertEquals(Strings.EMAIL_SEQUENCE_BEHIND_TOO_SHORT, inputValidator.emailValidation(stringWithMonkey))
    }

    @Test
    fun emailValidationForLongerStringWithMonkeyAndDot(){
        val stringWithMonkey = "example@example."
        assertEquals(Strings.EMAIL_TOO_SHORT_BEHIND_DOT, inputValidator.emailValidation(stringWithMonkey))
    }

    @Test
    fun emailValidationForLongerStringBehindDot(){
        val stringWithMonkey = "example@example.example"
        assertEquals(Strings.EMAIL_GOOD, inputValidator.emailValidation(stringWithMonkey))
    }

    @Test
    fun passwordValidationForEmptyString(){
        val emptyString = ""
        assertEquals(Strings.PASSWORD_TOO_SHORT, inputValidator.passwordValidation(emptyString))
    }

    @Test
    fun passwordValidationForSixCharString(){
        val sixCharString = "exampl"
        assertEquals(Strings.PASSWORD_MUST_HAVE_DIGIT, inputValidator.passwordValidation(sixCharString))
    }

    @Test
    fun passwordValidationForSixNumbersString(){
        val sixNumbersString = "123456"
        assertEquals(Strings.PASSWORD_MUST_HAVE_LETTER, inputValidator.passwordValidation(sixNumbersString))
    }

    @Test
    fun passwordValidationForNumbersAndLettersString(){
        val string = "example123"
        assertEquals(Strings.PASSWORD_GOOD, inputValidator.passwordValidation(string))
    }

    @Test
    fun passwordValidationForTwoDifferentStrings(){
        val first = "first123"
        val second = "second123"
        assertEquals(false, inputValidator.arePasswordsTheSame(first, second))
    }

    @Test
    fun passwordValidationForTwoSameStrings(){
        val first = "example123"
        val second = first
        assertEquals(true, inputValidator.arePasswordsTheSame(first, second))
    }

}
