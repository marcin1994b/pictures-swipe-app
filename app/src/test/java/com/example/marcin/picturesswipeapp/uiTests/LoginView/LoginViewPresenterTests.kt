package com.example.marcin.picturesswipeapp.uiTests.LoginView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginViewContracts
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginViewPresenter
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InOrder
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class LoginViewPresenterTests {

    private lateinit var presenter : LoginViewPresenter
    private lateinit var inOrder : InOrder
    @Mock private lateinit var networkProvider: NetworkProvider
    @Mock private lateinit var firebaseAuthProvider: FirebaseAuthProvider
    @Mock private lateinit var view: LoginViewContracts.LoginView

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = LoginViewPresenter(networkProvider, firebaseAuthProvider, view)
        inOrder = Mockito.inOrder(networkProvider, firebaseAuthProvider, view)
    }

    @Test
    fun testPresenterIsNotNull(){
        assertNotNull(presenter)
    }

    @Test
    fun testOnLoginButtonTapMethodWhenEmailIsEmpty(){
        val email = ""
        val password = "example123"
        presenter.onLoginButtonTap(email, password)
        verify(networkProvider, never()).isNetworkAvailable()
        verify(firebaseAuthProvider, never()).signIn(email, password)
        verify(view).showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
    }

    @Test
    fun testOnLoginButtonTapMethodWhenPasswordIsEmpty(){
        val email = "example@example.com"
        val password = ""
        presenter.onLoginButtonTap(email, password)
        verify(networkProvider, never()).isNetworkAvailable()
        verify(firebaseAuthProvider, never()).signIn(email, password)
        verify(view).showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
    }

    @Test
    fun testOnLoginButtonTapMethodWhenEmailAndPasswordAreEmpty(){
        val email = ""
        val password = ""
        presenter.onLoginButtonTap(email, password)
        verify(networkProvider, never()).isNetworkAvailable()
        verify(firebaseAuthProvider, never()).signIn(email, password)
        verify(view).showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
    }

    @Test
    fun testOnLoginButtonTapMethodWhenIsNoInternet(){
        val email = "example@example.com"
        val password = "example123"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onLoginButtonTap(email, password)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnLoginButtonTapMethodWhenSignInFailed(){
        val email = "example@example.com"
        val password = "example123"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.signIn(any(), any())).then {
            presenter.onFirebaseProviderResult(false, Consts.SIGN_IN_USER_ACCTION, "")
        }

        presenter.onLoginButtonTap(email, password)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityLoginButton(false)
        inOrder.verify(view).setVisibilityProgressBar(true)
        inOrder.verify(firebaseAuthProvider).signIn(email, password)
        inOrder.verify(view).showToast("")
        inOrder.verify(view).setVisibilityProgressBar(false)
        inOrder.verify(view).setVisibilityLoginButton(true)

    }

    @Test
    fun testOnLoginButtonTapMethodWhenSignInIsSuccessful(){
        val email = "example@example.com"
        val password = "example123"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.signIn(any(), any())).then {
            presenter.onFirebaseProviderResult(true, Consts.SIGN_IN_USER_ACCTION, "")
        }

        presenter.onLoginButtonTap(email, password)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityLoginButton(false)
        inOrder.verify(view).setVisibilityProgressBar(true)
        inOrder.verify(firebaseAuthProvider).signIn(email, password)
        inOrder.verify(view).startMainActivity()
        inOrder.verify(view).setVisibilityProgressBar(false)
        inOrder.verify(view).setVisibilityLoginButton(true)
    }

    @Test
    fun testOnNotRememberTextViewTapMethod(){
        presenter.onNotRememberTextViewTap()
        verify(view).startRemindPasswordActivity()
    }

    @Test
    fun testOnCreateAccountTextViewTapMethod(){
        presenter.onCreateAccountTextViewTap()
        verify(view).startCreateAccountActivity()
    }
}