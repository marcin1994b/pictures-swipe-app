package com.example.marcin.picturesswipeapp.uiTests.RemindPasswordView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordViewContracts
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordViewPresenter
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InOrder
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class RemindPasswordViewPresenterTests {

    @Mock private lateinit var networkProvider: NetworkProvider
    @Mock private lateinit var firebaseAuthProvider: FirebaseAuthProvider
    @Mock private lateinit var view: RemindPasswordViewContracts.RemindPasswordView
    private lateinit var presenter: RemindPasswordViewPresenter
    private lateinit var inOrder: InOrder

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = RemindPasswordViewPresenter(networkProvider, firebaseAuthProvider, view)
        inOrder = Mockito.inOrder(networkProvider, firebaseAuthProvider, view)
    }

    @Test
    fun testPreenterIsNotNull(){
        assertNotNull(presenter)
    }

    @Test
    fun testOnRemindButtonTapMethodWhenIsNoInternet(){
        val email = "example@example.com"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onRemindButtonTap(email)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        verify(view, never()).setVisibilityRemindProgressBar(true)
        verify(view, never()).setVisibilityRemindButton(false)
        verify(firebaseAuthProvider, never()).remindPassword(email)
    }

    @Test
    fun testOnRemindButtonTapMethodWhenEmailIsEmpty(){
        val email = ""
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)

        presenter.onRemindButtonTap(email)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
        verify(view, never()).setVisibilityRemindProgressBar(true)
        verify(view, never()).setVisibilityRemindButton(false)
        verify(firebaseAuthProvider, never()).remindPassword(email)
    }

    @Test
    fun testOnRemindButtonTapMethodWhenRemindEmailFailed(){
        val email = "example@example.com"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.remindPassword(email)).then {
            presenter.onFirebaseProviderResult(false, Consts.REMIND_PASSWORD_ACTION, "")
        }

        presenter.onRemindButtonTap(email)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRemindButton(false)
        inOrder.verify(view).setVisibilityRemindProgressBar(true)
        inOrder.verify(firebaseAuthProvider).remindPassword(email)
        inOrder.verify(view).showToast("")
        inOrder.verify(view).setVisibilityRemindProgressBar(false)
        inOrder.verify(view).setVisibilityRemindButton(true)
        verify(view, never()).startLoginActivity()
    }

    @Test
    fun testOnRemindButtonTapMethodWhenRemindEmailIsSuccessful(){
        val email = "example@example.com"
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.remindPassword(email)).then {
            presenter.onFirebaseProviderResult(true, Consts.REMIND_PASSWORD_ACTION, "")
        }

        presenter.onRemindButtonTap(email)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRemindButton(false)
        inOrder.verify(view).setVisibilityRemindProgressBar(true)
        inOrder.verify(firebaseAuthProvider).remindPassword(email)
        inOrder.verify(view).showToast(Strings.WE_SEND_YOUR_PASSWORD)
        inOrder.verify(view).startLoginActivity()
        inOrder.verify(view).setVisibilityRemindProgressBar(false)
        inOrder.verify(view).setVisibilityRemindButton(true)
    }
}