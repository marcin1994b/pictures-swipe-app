package com.example.marcin.picturesswipeapp.uiTests.MainView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.MainActivity.MainActivity
import com.example.marcin.picturesswipeapp.ui.MainActivity.MainViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class MainViewTests {

    private lateinit var view: MainActivity
    @Mock private lateinit var presenter: MainViewContracts.MainViewPresenter

    @Before
    fun setUp(){
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        MockitoAnnotations.initMocks(this)
        view = Robolectric.buildActivity(MainActivity::class.java).create().get()
        view.presenter = presenter
    }

    @Test
    fun testViewIsNotNull(){
        assertNotNull(view)
    }
}