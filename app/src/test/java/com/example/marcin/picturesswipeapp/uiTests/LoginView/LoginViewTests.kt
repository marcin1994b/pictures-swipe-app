package com.example.marcin.picturesswipeapp.uiTests.LoginView

import android.view.View
import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountActivity
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginViewContracts
import com.example.marcin.picturesswipeapp.ui.MainActivity.MainActivity
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordActivity
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_login.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class LoginViewTests {

    private lateinit var view: LoginActivity
    @Mock private lateinit var presenter: LoginViewContracts.LoginViewPresenter

    @Before
    fun setUp(){
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        MockitoAnnotations.initMocks(this)
        view = Robolectric.buildActivity(LoginActivity::class.java).create().get()
        view.presenter = presenter
    }

    @Test
    fun testViewIsNotNull(){
        assertNotNull(view)
    }

    @Test
    fun testSetVisibilityLoginButtonMethod(){
        view.setVisibilityLoginButton(true)
        assertEquals(View.VISIBLE, view.signin_button.visibility)
        view.setVisibilityLoginButton(false)
        assertEquals(View.INVISIBLE, view.signin_button.visibility)
    }

    @Test
    fun testSetVisibilityProgressBarMethod(){
        view.setVisibilityProgressBar(true)
        assertEquals(View.VISIBLE, view.loginProgressBar.visibility)
        view.setVisibilityProgressBar(false)
        assertEquals(View.INVISIBLE, view.loginProgressBar.visibility)
    }

    @Test
    fun testStartCreateAccountActivityMethod(){
        view.startCreateAccountActivity()
        val startedIntent = shadowOf(view).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertEquals(CreateAccountActivity::class.java, shadowIntent.intentClass)
    }

    @Test
    fun testStartRemindPasswordActivityMethod(){
        view.startRemindPasswordActivity()
        val startedIntent = shadowOf(view).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertEquals(RemindPasswordActivity::class.java, shadowIntent.intentClass)
    }

    @Test
    fun testStartMainActivityMethod(){
        view.startMainActivity()
        val startedIntent = shadowOf(view).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertEquals(MainActivity::class.java, shadowIntent.intentClass)
    }
}