package com.example.marcin.picturesswipeapp.uiTests.CreateAccountView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountViewContracts
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountViewPresenter
import com.nhaarman.mockito_kotlin.any
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config



@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class CreateAccountViewPresenterTests{

    private lateinit var presenter: com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountViewPresenter

    @Mock private lateinit var networkProvider: NetworkProvider
    @Mock private lateinit var firebaseAuthProvider: FirebaseAuthProvider
    @Mock private lateinit var createAccountView: CreateAccountViewContracts.CreateAccountView
    @Mock private lateinit var firebaseDataProvider: FirebaseDatabaseProvider

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = CreateAccountViewPresenter(networkProvider, firebaseAuthProvider, firebaseDataProvider,
                createAccountView)
    }

    @Test
    fun testOnHaveAccountTextViewTapMethod(){
        presenter.onHaveAccountTextViewTap()
        verify(createAccountView).startLoginActivity()
    }

    @Test
    @Throws(Exception::class)
    fun testOnCreateButtonTapMethodWhenNetworkIsAvailableAndCreateAccountIsSuccessful(){
        val email = "exampe@example.example"
        val pass = "example123"
        val inOrder = inOrder(createAccountView, firebaseAuthProvider, firebaseDataProvider, networkProvider)

        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)

        `when`(firebaseAuthProvider.createNewAccount(email, pass)).then {
            presenter.onFirebaseProviderResult(true,
                    Consts.CREATE_NEW_ACCOUNT_ACTION, "") }

        presenter.onCreateButtonTap(email, pass, pass)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(createAccountView).setVisibilityCreateButton(false)
        inOrder.verify(createAccountView).setVisibilityCreateProgressBar(true)
        inOrder.verify(firebaseAuthProvider).createNewAccount(email, pass)
        inOrder.verify(firebaseAuthProvider).sendEmailVerification()
        inOrder.verify(firebaseDataProvider).postUserAccountData(any())
        inOrder.verify(createAccountView).startMainActivity()
        inOrder.verify(createAccountView).setVisibilityCreateProgressBar(false)
        inOrder.verify(createAccountView).setVisibilityCreateButton(true)
    }

    @Test
    @Throws(Exception::class)
    fun testCreateAccountButtonWhenNetworkIsAvailableButCreateAccountFailed(){
        val email = "exampe@example.example"
        val pass = "example123"
        val inOrder = inOrder(createAccountView, firebaseAuthProvider, firebaseDataProvider, networkProvider)

        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)

        `when`(firebaseAuthProvider.createNewAccount(email, pass)).then {
            presenter.onFirebaseProviderResult(false,
                    Consts.CREATE_NEW_ACCOUNT_ACTION, "") }

        presenter.onCreateButtonTap(email, pass, pass)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(createAccountView).setVisibilityCreateButton(false)
        inOrder.verify(createAccountView).setVisibilityCreateProgressBar(true)
        inOrder.verify(firebaseAuthProvider).createNewAccount(email, pass)
        inOrder.verify(createAccountView).showToast("")
        inOrder.verify(createAccountView).setVisibilityCreateProgressBar(false)
        inOrder.verify(createAccountView).setVisibilityCreateButton(true)

        verify(firebaseAuthProvider, never()).sendEmailVerification()
        verify(firebaseDataProvider, never()).postUserAccountData(any())
        verify(createAccountView, never()).startMainActivity()
    }

    @Test
    @Throws(Exception::class)
    fun testCreateAccountButtonWhenNetworkIsNotAvailable(){
        val email = "exampe@example.example"
        val pass = "example123"
        val inOrder = inOrder(createAccountView, firebaseAuthProvider, firebaseDataProvider, networkProvider)

        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onCreateButtonTap(email, pass, pass)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(createAccountView).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)

        verify(createAccountView, never()).setVisibilityCreateButton(false)
        verify(createAccountView, never()).setVisibilityCreateProgressBar(true)
        verify(firebaseAuthProvider, never()).createNewAccount(email, pass)
    }
}
