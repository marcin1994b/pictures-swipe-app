package com.example.marcin.picturesswipeapp.uiTests.SettingsView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider
import com.example.marcin.picturesswipeapp.ui.SettingsView.SettingsViewContracts
import com.example.marcin.picturesswipeapp.ui.SettingsView.SettingsViewPresenter
import com.nhaarman.mockito_kotlin.verify
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InOrder
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class SettingsViewPresenterTests {

    @Mock private lateinit var sharedPreferencesProvider: SharedPreferencesProvider
    @Mock private lateinit var view: SettingsViewContracts.SettingsView
    private lateinit var presenter: SettingsViewPresenter
    private lateinit var inOrder: InOrder

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = SettingsViewPresenter(sharedPreferencesProvider, view)
        inOrder = Mockito.inOrder(sharedPreferencesProvider, view)
    }

    @Test
    fun testPresenterIsNotNull(){
        assertNotNull(presenter)
    }

    @Test
    fun testOnRadioGroupClickedWhenStackImgQualityCardViewSetFirstRadioButton(){
        val cardView = Consts.STACK_IMG_QUALITY_CARDVIEW
        val radioButton = 1

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.SMALL_IMAGES_QUALITY, AppConfig.stackImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.IMAGES_STACK_QUALITY_CONFIG_NAME,
                AppConfig.stackImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenStackImgQualityCardViewSetSecondRadioButton(){
        val cardView = Consts.STACK_IMG_QUALITY_CARDVIEW
        val radioButton = 2

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.MEDIUM_IMAGES_QUALITY, AppConfig.stackImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.IMAGES_STACK_QUALITY_CONFIG_NAME,
                AppConfig.stackImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenStackImgQualityCardViewSetThirdRadioButton(){
        val cardView = Consts.STACK_IMG_QUALITY_CARDVIEW
        val radioButton = 3

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.LARGE_IMAGES_QUALITY, AppConfig.stackImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.IMAGES_STACK_QUALITY_CONFIG_NAME,
                AppConfig.stackImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenFullImageQualityCardViewSetFirstRadioButton(){
        val cardView = Consts.FULL_IMG_QUALITY_CARDVIEW
        val radioButton = 1

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.SMALL_IMAGES_QUALITY, AppConfig.fullImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.FULL_IMAGES_QUALITY_CONFIG_NAME,
                AppConfig.fullImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenFullImageQualityCardViewSetSecondRadioButton(){
        val cardView = Consts.FULL_IMG_QUALITY_CARDVIEW
        val radioButton = 2

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.MEDIUM_IMAGES_QUALITY, AppConfig.fullImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.FULL_IMAGES_QUALITY_CONFIG_NAME,
                AppConfig.fullImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenFullImageQualityCardViewSetThirdRadioButton(){
        val cardView = Consts.FULL_IMG_QUALITY_CARDVIEW
        val radioButton = 3

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.LARGE_IMAGES_QUALITY, AppConfig.fullImagesQuality)
        verify(sharedPreferencesProvider).saveData(Consts.FULL_IMAGES_QUALITY_CONFIG_NAME,
                AppConfig.fullImagesQuality)
    }

    @Test
    fun testOnRadioGroupClickedWhenMaxStackSizeCardViewSetFirstRadioButton(){
        val cardView = Consts.MAX_STACK_SIZE_CARDVIEW
        val radioButton = 1

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.SMALL_MAX_STACK_SIZE, AppConfig.maxStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MAX_STACK_SIZE_CONFIG_NAME,
                AppConfig.maxStackSize)
    }

    @Test
    fun testOnRadioGroupClickedWhenMaxStackSizeCardViewSetSecondRadioButton(){
        val cardView = Consts.MAX_STACK_SIZE_CARDVIEW
        val radioButton = 2

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.MEDIUM_MAX_STACK_SIZE, AppConfig.maxStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MAX_STACK_SIZE_CONFIG_NAME,
                AppConfig.maxStackSize)
    }

    @Test
    fun testOnRadioGroupClickedWhenMaxStackSizeCardViewSetThirdRadioButton(){
        val cardView = Consts.MAX_STACK_SIZE_CARDVIEW
        val radioButton = 3

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.LARGE_MAX_STACK_SIZE, AppConfig.maxStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MAX_STACK_SIZE_CONFIG_NAME,
                AppConfig.maxStackSize)
    }

    @Test
    fun testOnRadioGroupClickedWhenMinStackSizeCardViewSetFirstRadioButton(){
        val cardView = Consts.MIN_STACK_SIZE_CARDVIEW
        val radioButton = 1

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.SMALL_MIN_STACK_SIZE, AppConfig.minStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MIN_STACK_SIZE_CONFIG_NAME,
                AppConfig.minStackSize)
    }

    @Test
    fun testOnRadioGroupClickedWhenMinStackSizeCardViewSetSecondRadioButton(){
        val cardView = Consts.MIN_STACK_SIZE_CARDVIEW
        val radioButton = 2

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.MEDIUM_MIN_STACK_SIZE, AppConfig.minStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MIN_STACK_SIZE_CONFIG_NAME,
                AppConfig.minStackSize)
    }

    @Test
    fun testOnRadioGroupClickedWhenMinStackSizeCardViewSetThirdRadioButton(){
        val cardView = Consts.MIN_STACK_SIZE_CARDVIEW
        val radioButton = 3

        presenter.onRadioGroupClicked(radioButton, cardView)
        assertEquals(AppConfig.LARGE_MIN_STACK_SIZE, AppConfig.minStackSize)
        verify(sharedPreferencesProvider).saveData(Consts.MIN_STACK_SIZE_CONFIG_NAME,
                AppConfig.minStackSize)
    }
}