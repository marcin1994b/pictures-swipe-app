package com.example.marcin.picturesswipeapp.uiTests.CreateAccountView

import android.view.View
import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountActivity
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertEquals
import kotlinx.android.synthetic.main.activity_create_account.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config



@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class CreateAccountViewTests{

    lateinit var createAccountView : CreateAccountActivity
    @Mock private lateinit var createAccountViewPresenter: CreateAccountViewContracts.CreateAccountPresenter

    @Before
    fun setUp() {
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        MockitoAnnotations.initMocks(this)
        createAccountView = Robolectric.buildActivity(CreateAccountActivity::class.java).create().get()
        //createAccountView.presenter = createAccountViewPresenter
    }

    @Test
    fun testSetVisibilityCreateButtonMethodForFalse(){
        createAccountView.setVisibilityCreateButton(false)
        assertEquals(View.INVISIBLE, createAccountView.create_button.visibility)
    }

    @Test
    fun testSetVisibilityCreateButtonMethodForTrue(){
        createAccountView.setVisibilityCreateButton(true)
        assertEquals(View.VISIBLE, createAccountView.create_button.visibility)
    }

    @Test
    fun testSetVisibilityCreateProgressBarMethodForTrue(){
        createAccountView.setVisibilityCreateProgressBar(true)
        assertEquals(View.VISIBLE, createAccountView.createAccountProgressBar.visibility)
    }

    @Test
    fun testSetVisibilityCreateProgressBarMethodForFalse(){
        createAccountView.setVisibilityCreateProgressBar(false)
        assertEquals(View.INVISIBLE, createAccountView.createAccountProgressBar.visibility)
    }

    @Test
    fun testSetEnabledCreateButtonMethodForTrue(){
        createAccountView.setEnabledCreateButton(true)
        assertEquals(true, createAccountView.create_button.isEnabled)
    }

    @Test
    fun testSetEnabledCreateButtonMethodForFalse(){
        createAccountView.setEnabledCreateButton(false)
        assertEquals(false, createAccountView.create_button.isEnabled)
    }

    @Test
    fun testShowEmailError(){
        val string = "example"
        createAccountView.showEmailError(string)
        assertEquals(string, createAccountView.email_wrapper.error.toString())
    }

    @Test
    fun testShowFirstPasswordError(){
        val string = "example"
        createAccountView.showFirstPasswordError(string)
        assertEquals(string, createAccountView.first_password_wrapper.error.toString())
    }

    @Test
    fun testShowSecondPasswordError(){
        val string = "example"
        createAccountView.showSecondPasswordError(string)
        assertEquals(string, createAccountView.second_password_wrapper.error.toString())
    }

}
