package com.example.marcin.picturesswipeapp.uiTests.StackView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.InfoView.InfoActivity
import com.example.marcin.picturesswipeapp.ui.StackView.StackFragment
import com.example.marcin.picturesswipeapp.ui.StackView.StackViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class StackViewTests {

    private lateinit var view: StackFragment
    @Mock private lateinit var presenter: StackViewContracts.MainViewPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        view = StackFragment()
        view.presenter = presenter
        startFragment(view)
    }

    @Test
    fun testViewIsNotNull(){
        assertNotNull(view)
    }

    @Test
    fun testStartInfoActivityMethod(){
        view.startInfoActivity()
        val startedIntent = shadowOf(view.activity).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertEquals(InfoActivity::class.java, shadowIntent.intentClass)
    }
}