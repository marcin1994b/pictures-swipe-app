package com.example.marcin.picturesswipeapp.uiTests.AccountView

import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.Model.InputValidator
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.AccountView.AccountViewContracts
import com.example.marcin.picturesswipeapp.ui.AccountView.AccountViewPresenter
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class AccountViewPresenterTests {

    private lateinit var presenter: AccountViewPresenter

    @Mock private lateinit var networkProvider: NetworkProvider
    @Mock private lateinit var firebaseAuthProvider: FirebaseAuthProvider
    @Mock private lateinit var view: AccountViewContracts.AccountView
    @Mock private lateinit var firebaseDataProvider: FirebaseDatabaseProvider
    @Mock private lateinit var inputValidator: InputValidator

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = AccountViewPresenter(view, inputValidator, firebaseAuthProvider, firebaseDataProvider, networkProvider)
    }

    @Test
    fun testOnVerifyArrowTapMethodWhenVerifyViewIsExpanded(){
        presenter.isFullVerifyViewVisible = true
        presenter.onVerifyViewArrowTap()
        verify(view).collapseVerifyView()
        assert(!presenter.isFullVerifyViewVisible)
    }

    @Test
    fun testOnVerifyArrowTapMethodWhenVerifyViewIsCollapsed(){
        presenter.isFullVerifyViewVisible = false
        presenter.onVerifyViewArrowTap()
        verify(view).expandVerifyView()
        assert(presenter.isFullVerifyViewVisible)
    }

    @Test
    fun testOnChangeEmailArrowTapMethodWhenChangeEmailViewIsExpanded(){
        presenter.isFullChangeEmailViewVivisble = true
        presenter.onChangeEmailViewArrowTap()
        verify(view).collapseChangeEmailView()
        assert(!presenter.isFullChangeEmailViewVivisble)
    }

    @Test
    fun testOnChangeEmailArrowTapMethodWhenChangeEmailViewIsCollapsed(){
        presenter.isFullChangeEmailViewVivisble = false
        presenter.onChangeEmailViewArrowTap()
        verify(view).expandChangeEmailView()
        assert(presenter.isFullChangeEmailViewVivisble)
    }

    @Test
    fun testOnChangePassArrowTapMethodWhenChangePassViewIsExpanded(){
        presenter.isFullChangePasswordViewVisible = true
        presenter.onChangePasswordViewArrowTap()
        verify(view).collapseChangePasswordView()
        assert(!presenter.isFullChangePasswordViewVisible)
    }

    @Test
    fun testOnChangePassArrowTapMethodWhenChangePassViewIsCollapsed(){
        presenter.isFullChangePasswordViewVisible = false
        presenter.onChangePasswordViewArrowTap()
        verify(view).expandChangePasswordView()
        assert(presenter.isFullChangePasswordViewVisible)
    }

    @Test
    fun testOnRestartAccountArrowTapMethodWhenRestartAccountViewIsExpanded(){
        presenter.isFullRestartAccountViewVisible = true
        presenter.onRestartAccountViewArrowTap()
        verify(view).collapseRestartAccountView()
        assert(!presenter.isFullRestartAccountViewVisible)
    }

    @Test
    fun testOnRestartAccountArrowTapMethodWhenRestartAccountViewIsCollapsed(){
        presenter.isFullRestartAccountViewVisible = false
        presenter.onRestartAccountViewArrowTap()
        verify(view).expandRestartAccountView()
        assert(presenter.isFullRestartAccountViewVisible)
    }

    @Test
    fun testOnRemoveAccountArrowTapMethodWhenRemoveAccountViewIsExpanded(){
        presenter.isFullRemoveAccountViewVisible = true
        presenter.onRemoveAccountViewArrowTap()
        verify(view).collapseRemoveAccountView()
        assert(!presenter.isFullRemoveAccountViewVisible)
    }

    @Test
    fun testOnRemoveAccountArrowTapMethodWhenRemoveAccountViewIsCollapsed(){
        presenter.isFullRemoveAccountViewVisible = false
        presenter.onRemoveAccountViewArrowTap()
        verify(view).expandRemoveAccountView()
        assert(presenter.isFullRemoveAccountViewVisible)
    }

    @Test
    fun testOnVerifyButtonTapMethodWhenIsNoInternet(){
        presenter.onVerifyViewButtonTap()
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnVerifyButtonTapMethodWhenInternetAvailableButSendEmailFailed(){
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.sendEmailVerification()).then {
            presenter.onFirebaseProviderResult(false,
                    Consts.SEND_EMAIL_VERIFICATION_ACTION, "")
        }

        presenter.onVerifyViewButtonTap()

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityVerifyButton(false)
        inOrder.verify(view).setVisibilityVerifyProgressBar(true)
        inOrder.verify(firebaseAuthProvider).sendEmailVerification()
        inOrder.verify(view).setVisibilityVerifyProgressBar(false)
        inOrder.verify(view).setVisibilityVerifyButton(true)
        inOrder.verify(view).showToast("")

    }

    @Test
    fun testOnVerifyButtonTapMethodWhenInternetAvailableAndSendEmailIsSuccessful(){
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.sendEmailVerification()).then {
            presenter.onFirebaseProviderResult(true,
                    Consts.SEND_EMAIL_VERIFICATION_ACTION, "")
        }

        presenter.onVerifyViewButtonTap()

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityVerifyButton(false)
        inOrder.verify(view).setVisibilityVerifyProgressBar(true)
        inOrder.verify(firebaseAuthProvider).sendEmailVerification()
        inOrder.verify(view).setVisibilityVerifyProgressBar(false)
        inOrder.verify(view).setVisibilityVerifyButton(true)
        inOrder.verify(view).showToast("OK")
    }

    @Test
    fun testOnChangeEmailViewButtonTapMethodWhenNoInternet(){
        val email = "example@example.com"
        val pass = "example123"
        presenter.onChangeEmailViewButtonTap(email, pass)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnChangeEmailViewButtonTapMethodWhenUpdateEmailFailed(){
        val email = "example@example.com"
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.updateEmail(any(), any())).then {
            presenter.onFirebaseProviderResult(false,
                    Consts.UPDATE_EMAIL_ACTION, "")
        }

        presenter.onChangeEmailViewButtonTap(email, pass)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityChangeEmailButton(false)
        inOrder.verify(view).setVisibilityChangeEmailProgressBar(true)
        inOrder.verify(firebaseAuthProvider).updateEmail(email, pass)
        inOrder.verify(view).setVisibilityChangeEmailProgressBar(false)
        inOrder.verify(view).setVisibilityChangeEmailButton(true)
        inOrder.verify(view).showToast("")
    }

    @Test
    fun testOnChangeEmailViewButtonTapMethodWhenUpdateEmailSuccessful(){
        val email = "example@example.com"
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.updateEmail(email, pass)).then {
            presenter.onFirebaseProviderResult(true,
                    Consts.UPDATE_EMAIL_ACTION, "")
        }

        presenter.onChangeEmailViewButtonTap(email, pass)
        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityChangeEmailButton(false)
        inOrder.verify(view).setVisibilityChangeEmailProgressBar(true)
        inOrder.verify(firebaseAuthProvider).updateEmail(email, pass)
        inOrder.verify(view).setVisibilityChangeEmailProgressBar(false)
        inOrder.verify(view).setVisibilityChangeEmailButton(true)
        inOrder.verify(view).showToast("OK")
    }

    @Test
    fun testOnChangePasswordViewButtonTapMethodWhenIsNoInternet(){
        val email = "example@example.com"
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onChangePasswordViewButtonTap(email, pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnChangePasswordViewButtonTapMethodWhenUpdatePasswordFailed(){
        val email = "example@example.com"
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.updatePassword(any(), any())).then {
            presenter.onFirebaseProviderResult(false, Consts.UPDATE_PASSWORD_ACTION, "")
        }

        presenter.onChangePasswordViewButtonTap(email, pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityChangePasswordButton(false)
        inOrder.verify(view).setVisibilityChangePasswordProgressBar(true)
        inOrder.verify(firebaseAuthProvider).updatePassword(email, pass)
        inOrder.verify(view).setVisibilityChangePasswordProgressBar(false)
        inOrder.verify(view).setVisibilityChangePasswordButton(true)
        inOrder.verify(view).showToast("")
    }

    @Test
    fun testOnChangePasswordViewButtonTapMethodWhenUpdatePasswordIsSuccessful(){
        val email = "example@example.com"
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.updatePassword(any(), any())).then {
            presenter.onFirebaseProviderResult(true, Consts.UPDATE_PASSWORD_ACTION, "")
        }

        presenter.onChangePasswordViewButtonTap(email, pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityChangePasswordButton(false)
        inOrder.verify(view).setVisibilityChangePasswordProgressBar(true)
        inOrder.verify(firebaseAuthProvider).updatePassword(email, pass)
        inOrder.verify(view).setVisibilityChangePasswordProgressBar(false)
        inOrder.verify(view).setVisibilityChangePasswordButton(true)
        inOrder.verify(view).showToast("OK")
    }

    @Test
    fun testOnRestartAccountViewButtonTapMethodWhenIsNoInternet(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onRestartAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnRestartAccountViewButtonTapMethodWhenRestartAccountFailed(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.reSignInUser(any())).then {
            presenter.onFirebaseProviderResult(false, Consts.RE_AUTH_USER_ACTION, "")
        }

        presenter.onRestartAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRestartAccountButton(false)
        inOrder.verify(view).setVisibilityRestartAccountProgressBar(true)
        inOrder.verify(firebaseAuthProvider).reSignInUser(pass)
        inOrder.verify(view).setVisibilityRestartAccountProgressBar(false)
        inOrder.verify(view).setVisibilityRestartAccountButton(true)
        inOrder.verify(view).showToast("")
    }

    @Test
    fun testOnRestartAccountViewButtonTapMethodWhenRestartAccountIsSuccessful(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.reSignInUser(any())).then {
            presenter.onFirebaseProviderResult(true, Consts.RE_AUTH_USER_ACTION, "")
        }

        presenter.onRestartAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRestartAccountButton(false)
        inOrder.verify(view).setVisibilityRestartAccountProgressBar(true)
        inOrder.verify(firebaseAuthProvider).reSignInUser(pass)
        inOrder.verify(view).setVisibilityRestartAccountProgressBar(false)
        inOrder.verify(view).setVisibilityRestartAccountButton(true)
        inOrder.verify(firebaseDataProvider).clearImagesList()
        inOrder.verify(firebaseDataProvider).updateAccountImageCounterData(0)
        inOrder.verify(view).showToast("OK")
    }

    @Test
    fun testOnRemoveAccountViewButtonTapMethodWhenIsNoInternet(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(false)

        presenter.onRemoveAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
    }

    @Test
    fun testOnRemoveAccountViewButtonTapMethodWhenRemoveAccountFailed(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.removeAccount(any())).then {
            presenter.onFirebaseProviderResult(false, Consts.REMOVE_ACCOUNT_ACTION, "")
        }


        presenter.onRemoveAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRemoveAccountButton(false)
        inOrder.verify(view).setVisibilityRemoveAccountProgressBar(true)
        inOrder.verify(firebaseAuthProvider).removeAccount(pass)
        inOrder.verify(view).setVisibilityRemoveAccountProgressBar(false)
        inOrder.verify(view).setVisibilityRemoveAccountButton(true)
        inOrder.verify(view).showToast("")
    }

    @Test
    fun testOnRemoveAccountViewButtonTapMethodWhenRemoveAccountIsSuccessful(){
        val pass = "example123"
        val inOrder = Mockito.inOrder(view, firebaseAuthProvider, firebaseDataProvider,
                networkProvider, inputValidator)
        `when`(networkProvider.isNetworkAvailable()).thenReturn(true)
        `when`(firebaseAuthProvider.removeAccount(any())).then {
            presenter.onFirebaseProviderResult(true, Consts.REMOVE_ACCOUNT_ACTION, "")
        }


        presenter.onRemoveAccountViewButtonTap(pass)

        inOrder.verify(networkProvider).isNetworkAvailable()
        inOrder.verify(view).setVisibilityRemoveAccountButton(false)
        inOrder.verify(view).setVisibilityRemoveAccountProgressBar(true)
        inOrder.verify(firebaseAuthProvider).removeAccount(pass)
        inOrder.verify(view).setVisibilityRemoveAccountProgressBar(false)
        inOrder.verify(view).setVisibilityRemoveAccountButton(true)
        inOrder.verify(view).showToast("OK")
        inOrder.verify(view).startSignInActivity()
    }





}