package com.example.marcin.picturesswipeapp.uiTests.RemindPasswordView

import android.view.View
import com.example.marcin.picturesswipeapp.BuildConfig
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordActivity
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordViewContracts
import com.google.firebase.FirebaseApp
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.android.synthetic.main.activity_remind_password.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
class RemindPasswordViewTests {

    private lateinit var view: RemindPasswordActivity
    @Mock private lateinit var presenter: RemindPasswordViewContracts.RemindPasswordViewPresenter

    @Before
    fun setUp(){
        FirebaseApp.initializeApp(RuntimeEnvironment.application)
        MockitoAnnotations.initMocks(this)
        view = Robolectric.buildActivity(RemindPasswordActivity::class.java).create().get()
        view.presenter = presenter
    }

    @Test
    fun testViewIsNotNull(){
        assertNotNull(view)
    }

    @Test
    fun testStartLoginActivityMethod(){
        view.startLoginActivity()
        val startedIntent = shadowOf(view).nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertEquals(LoginActivity::class.java, shadowIntent.intentClass)
    }

    @Test
    fun testSetVisibilityRemindButton(){
        view.setVisibilityRemindButton(true)
        assertEquals(View.VISIBLE, view.remind_button.visibility)
        view.setVisibilityRemindButton(false)
        assertEquals(View.INVISIBLE, view.remind_button.visibility)
    }

    @Test
    fun testSetVisibilityRemindProgressBar(){
        view.setVisibilityRemindProgressBar(true)
        assertEquals(View.VISIBLE, view.remindProgressBar.visibility)
        view.setVisibilityRemindProgressBar(false)
        assertEquals(View.INVISIBLE, view.remindProgressBar.visibility)
    }
}