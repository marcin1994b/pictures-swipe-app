package com.example.marcin.picturesswipeapp.ui.MainActivity

interface MainViewContracts {

    interface MainView{

        fun startStackView()
        fun startLikedImagesView()
        fun startAccountView()
        fun startSettingsView()
        fun startSignInView()
        fun closeNavDrawer()
        fun showToast(msg: String)
        fun setNavDrawerTextView(text: String)
    }

    interface MainViewPresenter{

        fun onViewCreated()
        fun onNavStackViewItemTap()
        fun onNavLikedImagesViewItemTap()
        fun onNavAccountViewItemTap()
        fun onNavSettingsViewItemTap()
        fun onNavSignOutItemTap()
    }
}