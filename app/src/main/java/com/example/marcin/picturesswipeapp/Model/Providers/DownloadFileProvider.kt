package com.example.marcin.picturesswipeapp.Model.Providers

import android.app.DownloadManager
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Environment


class DownloadFileProvider(private val context: Context) {

    fun downloadFile(fileUrl: String, fileName: String){
        val downloadManagerRequest = DownloadManager.Request(Uri.parse(fileUrl))
        val dm = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        downloadManagerRequest.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, fileName)
        downloadManagerRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        downloadManagerRequest.allowScanningByMediaScanner()
        dm.enqueue(downloadManagerRequest)
    }
}