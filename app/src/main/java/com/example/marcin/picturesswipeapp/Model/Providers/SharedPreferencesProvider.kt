package com.example.marcin.picturesswipeapp.Model.Providers

import android.app.Activity
import android.content.Context



class SharedPreferencesProvider(context: Context) {

    val PREFERENCES_NAME = "com.marcinbaranski.swipeappconfig"
    val IMAGES_STACK_QUALITY_CONFIG_NAME = "imagesStackQuality"
    val FULL_IMAGES_QUALITY_CONFIG_NAME = "fullImagesQuality"
    val MAX_STACK_SIZE_CONFIG_NAME = "maxStackSize"
    val MIN_STACK_SIZE_CONFIG_NAME = "minStackSize"
    val DEFAULT_VALUE = 0

    private val preferences = context.getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);

    fun saveData(dataName: String, value: Int){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putInt(dataName, value)
        preferencesEditor.commit()
    }

    fun restoreData(dataName: String): Int {
        return preferences.getInt(dataName, DEFAULT_VALUE)
    }
}