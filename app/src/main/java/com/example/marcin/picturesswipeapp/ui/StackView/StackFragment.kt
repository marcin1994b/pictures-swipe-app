package com.example.marcin.picturesswipeapp.ui.StackView

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Adapters.SwipeStackAdapter
import com.example.marcin.picturesswipeapp.Model.Providers.ApiProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.InfoView.InfoActivity
import com.example.marcin.picturesswipeapp.ui.SwipeStackViewHolder
import kotlinx.android.synthetic.main.fragment_stack_view.*
import kotlinx.android.synthetic.main.fragment_stack_view.view.*
import link.fls.swipestack.SwipeStack

class StackFragment : Fragment(), StackViewContracts.MainView {

    var presenter : StackViewContracts.MainViewPresenter? = null
    private lateinit var layout: View

    companion object {
        fun newInstance(): Fragment = StackFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layout = inflater!!.inflate(R.layout.fragment_stack_view, container, false)
        initSwipeStackView()
        initPresenter()
        showProgressBar()
        presenter?.onViewCreated()
        return layout
    }

    override fun swipeTopViewToLeft() {
        Log.d("StackFragment", "swipe top view to left")
        layout.swipeStack.swipeTopViewToLeft()
    }

    override fun swipeTopViewToRight() {
        Log.d("StackFragment", "swipe top view to right")
        layout.swipeStack.swipeTopViewToRight()
    }

    override fun undoSwipe() {
        swipeStack.resetStack()
    }

    override fun startInfoActivity() {
        Log.d("StackFragment", "start info activity")
        val intent = Intent(this.context, InfoActivity::class.java)
        intent.putExtra("imgPosition", layout.swipeStack.currentPosition)
        intent.putExtra("isFromApi", false)
        startActivity(intent)

    }

    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        stackViewProgressBar?.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        stackViewProgressBar?.visibility = View.GONE
    }

    private fun initPresenter(){
        if(presenter == null) {
            presenter = StackViewPresenter(NetworkProvider(this.context),
                    ApiProvider(), this, FirebaseDatabaseProvider())
        }
    }

    private fun initSwipeStackView(){
        SwipeStackViewHolder.swipeStackAdapter = SwipeStackAdapter(this.context)
        layout.swipeStack.let{
            it.setListener(getSwipeListener())
            it.setSwipeProgressListener(getSwipeProgressListener())
            it.adapter = SwipeStackViewHolder.swipeStackAdapter
        }
    }

    private fun getSwipeListener() = object : SwipeStack.SwipeStackListener {
        override fun onViewSwipedToLeft(position: Int) {
            presenter?.onSwipedToLeft(position)
        }

        override fun onViewSwipedToRight(position: Int) {
            presenter?.onSwipedToRight(position)
        }

        override fun onStackEmpty() {
            presenter?.onEmptyStack()
        }
    }

    private fun getSwipeProgressListener() = object : SwipeStack.SwipeProgressListener {
        override fun onSwipeEnd(position: Int) {
            presenter?.onSwipeEnd(position)
        }

        override fun onSwipeStart(position: Int) {
            presenter?.onSwipeStart(position)
        }

        override fun onSwipeProgress(position: Int, progress: Float) {
            presenter?.onSwipeProgress(position, progress)
        }
    }

}
