package com.example.marcin.picturesswipeapp.ui.LikedImagesView

interface LikedImagesViewContracts {

    interface FavImagesView{

        fun showToast(msg: String)
        fun startInfoActivity(position: Int)
        fun notifyAdapterDataSetChanged()
    }

    interface FavImagesViewPresenter{

        fun onRecyclerListItemTap(position: Int)
        fun downloadImagesList(page: Int)
        fun onViewCreated()
    }
}