package com.example.marcin.picturesswipeapp.ui.CreateAccountView

import io.reactivex.Observable

interface CreateAccountViewContracts {

    interface CreateAccountView{

        fun startLoginActivity()
        fun showToast(msg: String)
        fun showFirstPasswordError(err: String)
        fun showSecondPasswordError(err: String)
        fun showEmailError(err: String)
        fun setEnabledCreateButton(isEnabled: Boolean)
        fun setVisibilityCreateButton(isVisible: Boolean)
        fun setVisibilityCreateProgressBar(isVisible: Boolean)
    }

    interface CreateAccountPresenter{

        fun onCreated(emailObservable: Observable<CharSequence>, firstPasswordObservable: Observable<CharSequence>,
                      secondPasswordObservable: Observable<CharSequence>)
        fun onCreateButtonTap(email: String, firstPassword: String, secondPassword: String)
        fun onHaveAccountTextViewTap()
    }
}