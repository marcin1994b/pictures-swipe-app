package com.example.marcin.picturesswipeapp.ui.CreateAccountView

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.activity_create_account.*


class CreateAccountActivity() : AppCompatActivity(), CreateAccountViewContracts.CreateAccountView {

    lateinit var presenter : CreateAccountViewContracts.CreateAccountPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        initPresenter()
        initListener()
        initTextViewSpan()
    }

    override fun startLoginActivity() {
        Log.d("CreateAccountActivity", "start login activity")
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun setEnabledCreateButton(isEnabled: Boolean) {
        create_button.isEnabled = isEnabled
    }

    override fun setVisibilityCreateButton(isVisible: Boolean) =
            when(isVisible){
                true -> create_button.visibility = View.VISIBLE
                false-> create_button.visibility = View.INVISIBLE
            }


    override fun setVisibilityCreateProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> createAccountProgressBar.visibility = View.VISIBLE
                false-> createAccountProgressBar.visibility = View.INVISIBLE
            }


    override fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showFirstPasswordError(err: String) {
        first_password_wrapper.error = err
    }

    override fun showSecondPasswordError(err: String) {
        second_password_wrapper.error = err
    }

    override fun showEmailError(err: String) {
        email_wrapper.error = err
    }

    private fun initPresenter(){
        presenter = CreateAccountViewPresenter(NetworkProvider(applicationContext),
                FirebaseAuthProvider(this), FirebaseDatabaseProvider(),  this)
        presenter?.onCreated(email_edittext.textChanges(), first_password_edittext.textChanges(),
                second_password_edittext.textChanges())
    }

    private fun initTextViewSpan(){
        val content = SpannableString(Strings.I_HAVE_ALREADY_ACCOUNT)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        have_already_account_textview.text = content
    }

    private fun initListener(){
        create_button.setOnClickListener { presenter.onCreateButtonTap(email_edittext.text.toString(),
                first_password_edittext.text.toString(), second_password_edittext.text.toString()) }
        have_already_account_textview.setOnClickListener { presenter.onHaveAccountTextViewTap() }
    }

}