package com.example.marcin.picturesswipeapp.ui.LikedImagesView

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Adapters.ImagesListAdapter
import com.example.marcin.picturesswipeapp.Model.EndlessScrollListener
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.FavImgListAdapterHolder
import com.example.marcin.picturesswipeapp.ui.InfoView.InfoActivity
import kotlinx.android.synthetic.main.fragment_liked_images.view.*

class LikedImagesFragment : Fragment(), LikedImagesViewContracts.FavImagesView {

    private lateinit var presenter : LikedImagesViewContracts.FavImagesViewPresenter
    private lateinit var layout: View

    companion object {
        fun newInstance(): Fragment = LikedImagesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layout = inflater!!.inflate(R.layout.fragment_liked_images, container, false)
        initPresenter()
        initListAdapter()
        initRecyclerView()
        presenter.onViewCreated()
        return layout
    }

    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun startInfoActivity(position: Int) {
        val intent = Intent(this.context, InfoActivity::class.java)
        intent.putExtra("imgPosition", position)
        intent.putExtra("isFromApi", true)
        startActivity(intent)
    }

    override fun notifyAdapterDataSetChanged() {
        layout.recycler_view.adapter.notifyDataSetChanged()
    }

    private fun initPresenter(){
        presenter = LikedImagesViewPresenter(NetworkProvider(this.context),
                FirebaseDatabaseProvider(), this)
    }

    private fun initListAdapter(){
        FavImgListAdapterHolder.adapter = ImagesListAdapter(mutableListOf()){
            presenter.onRecyclerListItemTap(it)
        }
    }

    private fun initRecyclerView(){
        var layoutManager  = GridLayoutManager(this.context, 3)
        layout.recycler_view.layoutManager = layoutManager
        layout.recycler_view.adapter = FavImgListAdapterHolder.adapter
        layout.recycler_view.addOnScrollListener(endlessScrollListener())
    }

    private fun endlessScrollListener() =
        object : EndlessScrollListener(layout.recycler_view.layoutManager as GridLayoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.downloadImagesList(page)
            }
        }

}

