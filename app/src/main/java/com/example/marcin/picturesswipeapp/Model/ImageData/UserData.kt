package com.example.marcin.picturesswipeapp.Model.ImageData

class UserData(){

    constructor(id: String, username: String, name: String, profile_image: ProfileImage,
                links: Links) : this(){
        this.id = id
        this.username = username
        this.name = name
        this.profile_image = profile_image
        this.links = links
    }

    var id: String = ""
    var username: String = ""
    var name: String = ""
    var profile_image: ProfileImage = ProfileImage()
    var links: Links = Links()
}
