package com.example.marcin.picturesswipeapp.ui.LoginView

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.CreateAccountView.CreateAccountActivity
import com.example.marcin.picturesswipeapp.ui.MainActivity.MainActivity
import com.example.marcin.picturesswipeapp.ui.RemindPasswordView.RemindPasswordActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginViewContracts.LoginView {

    lateinit var presenter: LoginViewContracts.LoginViewPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListeners()
        initTextViewsSpan()
        initPresenter()
    }

    override fun startCreateAccountActivity() {
        Log.d("LoginActivity", "start create account activity")
        val intent = Intent(this, CreateAccountActivity::class.java)
        startActivity(intent)
    }

    override fun startRemindPasswordActivity() {
        Log.d("LoginActivity", "start remind password activity")
        val intent = Intent(this, RemindPasswordActivity::class.java)
        startActivity(intent)
    }

    override fun startMainActivity() {
        Log.d("LoginActivity", "start main activity")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun showToast(msg: String) =
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    override fun setVisibilityLoginButton(isVisible: Boolean) =
            when(isVisible){
                true -> signin_button.visibility = View.VISIBLE
                false-> signin_button.visibility = View.INVISIBLE
            }

    override fun setVisibilityProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> loginProgressBar.visibility = View.VISIBLE
                false-> loginProgressBar.visibility = View.INVISIBLE
            }


    private fun initPresenter(){
        presenter = LoginViewPresenter(NetworkProvider(applicationContext),
                FirebaseAuthProvider(this), this)
    }

    private fun initListeners(){
        signin_button.setOnClickListener { presenter.onLoginButtonTap(email_edittext.text.toString(),
                password_edittext.text.toString()) }
        do_not_remember_textview.setOnClickListener { presenter.onNotRememberTextViewTap() }
        create_accout_textview.setOnClickListener { presenter.onCreateAccountTextViewTap() }
    }

    private fun initTextViewsSpan(){
        var content = SpannableString(Strings.I_DO_NOT_REMEMBER_PASSWORD)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        do_not_remember_textview.text = content

        content = SpannableString(Strings.CREATE_NEW_ACCOUNT)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        create_accout_textview.text = content
    }

}