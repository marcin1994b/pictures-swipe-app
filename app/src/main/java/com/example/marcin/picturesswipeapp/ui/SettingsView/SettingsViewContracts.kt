package com.example.marcin.picturesswipeapp.ui.SettingsView


interface SettingsViewContracts {

    interface SettingsView {

        fun showToast(msg: String)
        fun setStackImagesQualityCardView(checked: Int)
        fun setFullImageQualityCardView(checked: Int)
        fun setMaxSizeOfStackCardView(checked: Int)
        fun setMinSizeOfStackCardView(checked: Int)
    }

    interface SettingsViewPresenter {

        fun onViewCreated()
        fun onRadioGroupClicked(radioButton: Int, cardView: Int)
    }
}