package com.example.marcin.picturesswipeapp.ui.LikedImagesView

import com.example.marcin.picturesswipeapp.Model.ImagesListData
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.FavImgListAdapterHolder

class LikedImagesViewPresenter(private val networkProvider: NetworkProvider,
                               private val firebaseDatabaseProvider: FirebaseDatabaseProvider,
                               private val view: LikedImagesViewContracts.FavImagesView)
    : LikedImagesViewContracts.FavImagesViewPresenter, FirebaseDatabaseProvider.OnFirebaseDatabaseProviderResult {

    override fun onViewCreated() =
            when(networkProvider.isNetworkAvailable()){
                true -> downloadImagesList(0)
                false -> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
            }

    override fun onRecyclerListItemTap(position: Int) {
        view.startInfoActivity(position)
    }

    override fun onFirebaseDatabaseProviderGetResult(isSuccess: Boolean, any: Any?, msg: String) {
        when (isSuccess) {
            true -> addImagesToListAdapter(any)
            false -> view.showToast(msg)
        }
    }

    override fun downloadImagesList(page: Int){
        firebaseDatabaseProvider.presenter = this
        firebaseDatabaseProvider.getImagesList(page)
    }

    private fun addImagesToListAdapter(any: Any?){
        any?.let{
            (any as ImagesListData).imagesList.let{
                println(it.keys.toString())
            }

            FavImgListAdapterHolder.adapter?.imagesList?.addAll((any as ImagesListData).hashMapToList())
            view.notifyAdapterDataSetChanged()
        }
    }
}