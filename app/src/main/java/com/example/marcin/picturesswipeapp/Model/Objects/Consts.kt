package com.example.marcin.picturesswipeapp.Model.Objects

object Consts {

    val ORIENTATION_PORTRAIT = 1
    val ORIENTATION_LANDSCAPE = 2

    val INTENT_ACTION_TYPE = "text/plain"

    val STACK_IMG_QUALITY_CARDVIEW = 11
    val FULL_IMG_QUALITY_CARDVIEW = 21
    val MAX_STACK_SIZE_CARDVIEW = 31
    val MIN_STACK_SIZE_CARDVIEW = 41

    // must be tha same as in FirebaseAuthProvider
    val CREATE_NEW_ACCOUNT_ACTION = 1
    val SIGN_IN_USER_ACCTION = 2
    val REMIND_PASSWORD_ACTION = 3
    val SIGN_OUT_USER_ACTION = 4
    val RE_AUTH_USER_ACTION = 5
    val UPDATE_EMAIL_ACTION = 6
    val UPDATE_PASSWORD_ACTION = 7
    val REMOVE_ACCOUNT_ACTION = 8
    val SEND_EMAIL_VERIFICATION_ACTION = 9

    val IMAGES_STACK_QUALITY_CONFIG_NAME = "imagesStackQuality"
    val FULL_IMAGES_QUALITY_CONFIG_NAME = "fullImagesQuality"
    val MAX_STACK_SIZE_CONFIG_NAME = "maxStackSize"
    val MIN_STACK_SIZE_CONFIG_NAME = "minStackSize"
}