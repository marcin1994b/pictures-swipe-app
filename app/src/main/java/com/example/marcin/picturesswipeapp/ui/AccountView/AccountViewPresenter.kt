package com.example.marcin.picturesswipeapp.ui.AccountView

import com.example.marcin.picturesswipeapp.Model.InputValidator
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class AccountViewPresenter(private val view: AccountViewContracts.AccountView,
                           private val inputValidator: InputValidator,
                           private val firebaseAuthProvider: FirebaseAuthProvider,
                           private val firebaseDatabaseProvider: FirebaseDatabaseProvider,
                           private val networkProvider: NetworkProvider):
        AccountViewContracts.AccountViewPresenter, FirebaseAuthProvider.OnFirebaseProvider,
        FirebaseDatabaseProvider.OnFirebaseDatabaseProviderResult {

    override fun onFirebaseDatabaseProviderGetResult(isSuccess: Boolean, any: Any?, msg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var isFullVerifyViewVisible = true
    var isFullChangeEmailViewVivisble = false
    var isFullChangePasswordViewVisible = false
    var isFullRestartAccountViewVisible = false
    var isFullRemoveAccountViewVisible = false

    override fun onViewCreated() {
        view.expandVerifyView()
        view.collapseChangeEmailView()
        view.collapseChangePasswordView()
        view.collapseRestartAccountView()
        view.collapseRemoveAccountView()
        view.setVerifyView(firebaseAuthProvider.isUserEmailVerified())
        view.setRemoveAccountView()

        view.setEnabledChangeEmailButton(false)
        view.setEnabledChangePasswordButton(false)
        view.setEnabledRestartAccountButton(false)
        view.setEnabledRemoveAccountButton(false)

        firebaseAuthProvider.presenter = this
        firebaseDatabaseProvider.presenter = this
    }

    override fun onVerifyViewArrowTap() {
        when (isFullVerifyViewVisible) {
            true -> collapseVerifyView()
            false -> expandVerifyView()
        }
    }

    override fun onChangeEmailViewArrowTap() {
        when (isFullChangeEmailViewVivisble) {
            true -> collapseChangeEmailView()
            false -> expandChangeEmailView()
        }
    }

    override fun onChangePasswordViewArrowTap() {
        when (isFullChangePasswordViewVisible) {
            true -> collapseChangePasswordView()
            false -> expandChangePasswordView()
        }
    }

    override fun onRestartAccountViewArrowTap() {
        when (isFullRestartAccountViewVisible) {
            true -> collapseRestartAccountView()
            false -> expandRestartAccountView()
        }
    }

    override fun onRemoveAccountViewArrowTap() {
        when (isFullRemoveAccountViewVisible) {
            true -> collapseRemoveAccountView()
            false -> expandRemoveAccountView()
        }
    }

    override fun observChangeEmailView(newEmailObserver: Observable<CharSequence>,
                                               currentPassObserver: Observable<CharSequence>) {
        Observable
                .combineLatest(newEmailObserver, currentPassObserver,
                        BiFunction<CharSequence, CharSequence, Pair<String, String>> {
                            newEmail, currentPassword ->
                            Pair(newEmail.toString(), currentPassword.toString())
                        })
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    newEmailValidation(it.first)
                    view.setEnabledChangeEmailButton(shouldUnlockChangeEmailButton(it.first, it.second))
                }
    }

    override fun observChangePasswordView(currentPassword: Observable<CharSequence>,
                      firstPass: Observable<CharSequence>, secondPass: Observable<CharSequence>) {
        Observable
                .combineLatest(currentPassword, firstPass, secondPass,
                        Function3<CharSequence, CharSequence, CharSequence, MutableList<String>> {
                            currPass, firPass, secPass ->
                            mutableListOf(currPass.toString(), firPass.toString(), secPass.toString())
                        })
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    firstPassValidation(it[1])
                    secondPassValidation(it[1], it[2])
                    view.setEnabledChangePasswordButton(shouldUnlockChangePassButton(it[0], it[1], it[2]))
                }
    }

    override fun observRestartAccountView(currentPass: Observable<CharSequence>) {
        currentPass.debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .map { it.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    view.setEnabledRestartAccountButton(it.isNotBlank())
                }
    }

    override fun observRemoveAccountView(currentPass: Observable<CharSequence>) {
        currentPass.debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .map { it.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    view.setEnabledRemoveAccountButton(it.isNotBlank())
                }
    }


    override fun onVerifyViewButtonTap() {
        when(networkProvider.isNetworkAvailable()) {
           true -> {
               view.setVisibilityVerifyButton(false)
               view.setVisibilityVerifyProgressBar(true)
               firebaseAuthProvider.sendEmailVerification()
           }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onChangeEmailViewButtonTap(newEmail: String, password: String) {
        when(networkProvider.isNetworkAvailable()) {
            true -> {
                view.setVisibilityChangeEmailButton(false)
                view.setVisibilityChangeEmailProgressBar(true)
                firebaseAuthProvider.updateEmail(newEmail, password)
            }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onChangePasswordViewButtonTap(currentPass: String, newPassword: String) {
        when(networkProvider.isNetworkAvailable()) {
            true -> {
                view.setVisibilityChangePasswordButton(false)
                view.setVisibilityChangePasswordProgressBar(true)
                firebaseAuthProvider.updatePassword(currentPass, newPassword)
            }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onRestartAccountViewButtonTap(currentPass: String) {
        when(networkProvider.isNetworkAvailable()) {
            true -> {
                view.setVisibilityRestartAccountButton(false)
                view.setVisibilityRestartAccountProgressBar(true)
                firebaseAuthProvider.reSignInUser(currentPass)
            }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onRemoveAccountViewButtonTap(currentPass: String) {
        when(networkProvider.isNetworkAvailable()) {
            true -> {
                view.setVisibilityRemoveAccountButton(false)
                view.setVisibilityRemoveAccountProgressBar(true)
                firebaseAuthProvider.removeAccount(currentPass)
            }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String) {
        when(action){
            Consts.SEND_EMAIL_VERIFICATION_ACTION ->
                onSendEmailVerificationActionResult(isSuccess, msg)
            Consts.UPDATE_EMAIL_ACTION ->
                onUpdateEmailActionResult(isSuccess, msg)
            Consts.UPDATE_PASSWORD_ACTION ->
                onUpdatePasswordActionResult(isSuccess, msg)
            Consts.RE_AUTH_USER_ACTION ->
                onReAuthAccountActionResult(isSuccess, msg)
            Consts.REMOVE_ACCOUNT_ACTION ->
                onRemoveAccountActionResult(isSuccess, msg)
        }
    }

    private fun shouldUnlockChangeEmailButton(email: String, password: String): Boolean =
            (inputValidator.emailValidation(email).isBlank() && password.isNotBlank() &&
                    email.isNotBlank())

    private fun newEmailValidation(email: String){
        var emailValidationResult = ""
        when(email.isNotBlank()){
            true -> emailValidationResult = inputValidator.emailValidation(email)
        }
        view.setNewEmailWrapperErrorInChangeEmailView(emailValidationResult)
    }

    private fun shouldUnlockChangePassButton(currPass: String, firstPass: String, secPass: String)
            : Boolean = (currPass.isNotBlank() && firstPass.isNotBlank() && secPass.isNotBlank() &&
            firstPass == secPass && inputValidator.passwordValidation(firstPass).isEmpty())

    private fun firstPassValidation(firstPass: String){
        var firstPassValidationResult = ""
        when(firstPass.isNotBlank()){
            true -> firstPassValidationResult = inputValidator.passwordValidation(firstPass)
        }
        view.setFirstNewPassWrapperErrorInChangePasswordView(firstPassValidationResult)
    }

    private fun secondPassValidation(firstPass: String, secondPass: String){
        when(secondPass.isNotBlank() && secondPass != firstPass){
            true -> view.setSecondNewPassWrapperErrorInChangePasswordView(
                    Strings.PASSWORD_ARE_DIFFERENT)
            false -> view.setSecondNewPassWrapperErrorInChangePasswordView(
                    Strings.EMPTY_STRING)
        }
    }

    private fun onSendEmailVerificationActionResult(isSuccess: Boolean, msg: String) {
        view.setVisibilityVerifyProgressBar(false)
        view.setVisibilityVerifyButton(true)
        when(isSuccess){
            true -> view.showToast("OK")
            false-> view.showToast(msg)
        }
    }

    private fun onUpdateEmailActionResult(isSuccess: Boolean, msg: String){
        println("KUPA")
        view.setVisibilityChangeEmailProgressBar(false)
        view.setVisibilityChangeEmailButton(true)
        when(isSuccess){
            true -> view.showToast("OK")
            false-> view.showToast(msg)
        }
    }

    private fun onUpdatePasswordActionResult(isSuccess: Boolean, msg: String){
        view.setVisibilityChangePasswordProgressBar(false)
        view.setVisibilityChangePasswordButton(true)
        when(isSuccess){
            true -> view.showToast("OK")
            false-> view.showToast(msg)
        }
    }

    private fun onReAuthAccountActionResult(isSuccess: Boolean, msg: String){
        view.setVisibilityRestartAccountProgressBar(false)
        view.setVisibilityRestartAccountButton(true)
        when(isSuccess){
            true -> handleRestartAccount()
            false-> view.showToast(msg)
        }
    }

    private fun onRemoveAccountActionResult(isSuccess: Boolean, msg: String){
        view.setVisibilityRemoveAccountProgressBar(false)
        view.setVisibilityRemoveAccountButton(true)
        when(isSuccess){
            true -> {
                view.showToast("OK")
                view.startSignInActivity()
            }
            false-> view.showToast(msg)
        }
    }

    private fun handleRestartAccount(){
        when(networkProvider.isNetworkAvailable()){
            true -> {
                firebaseDatabaseProvider.clearImagesList()
                firebaseDatabaseProvider.updateAccountImageCounterData(0)
                firebaseDatabaseProvider.updateAccountLikedImagesCounterData(0)
                view.showToast("OK")
            }
            false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }
    //

    private fun expandVerifyView() {
        view.expandVerifyView()
        isFullVerifyViewVisible = true
    }

    private fun expandChangeEmailView() {
        view.expandChangeEmailView()
        isFullChangeEmailViewVivisble = true
    }

    private fun expandChangePasswordView() {
        view.expandChangePasswordView()
        isFullChangePasswordViewVisible = true
    }

    private fun expandRestartAccountView() {
        view.expandRestartAccountView()
        isFullRestartAccountViewVisible = true
    }

    private fun expandRemoveAccountView() {
        view.expandRemoveAccountView()
        view.setVisibilityRemoveAccountProgressBar(false)
        isFullRemoveAccountViewVisible = true
    }

    private fun collapseVerifyView() {
        view.collapseVerifyView()
        isFullVerifyViewVisible = false
    }

    private fun collapseChangeEmailView() {
        view.collapseChangeEmailView()
        isFullChangeEmailViewVivisble = false
    }

    private fun collapseChangePasswordView() {
        view.collapseChangePasswordView()
        isFullChangePasswordViewVisible = false
    }

    private fun collapseRestartAccountView() {
        view.collapseRestartAccountView()
        isFullRestartAccountViewVisible = false
    }

    private fun collapseRemoveAccountView() {
        view.collapseRemoveAccountView()
        isFullRemoveAccountViewVisible = false
    }
}