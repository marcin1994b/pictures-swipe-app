package com.example.marcin.picturesswipeapp.Model.ImageData

class Links(){

    constructor(html: String, download: String): this(){
        this.html = html
        this.download = download
    }

    var html: String = ""
    var download: String = ""
}