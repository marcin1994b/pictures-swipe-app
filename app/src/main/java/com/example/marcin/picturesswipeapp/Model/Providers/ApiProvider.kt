package com.example.marcin.picturesswipeapp.Model.Providers

import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

class ApiProvider {

    private interface ApiInterface {

        @GET("photos")
        fun getPhotos(@Query("client_id") client_id: String,
                      @Query("per_page") per_page: Int,
                      @Query("page") page: Int,
                      @Query("order_by") order: String): Single<List<ImageData>>
    }

    private object APIClient {

        val BASIC_URL : String = "https://api.unsplash.com/"
        var retrofit : Retrofit

        init {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASIC_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }

    private val CLIENT_ID : String = ""
    private val apiService : ApiInterface = APIClient.retrofit.create(ApiInterface::class.java)

    fun getPhotos(perPage: Int, page : Int, order: String) : Single<List<ImageData>> =
            apiService.getPhotos(CLIENT_ID, perPage, page, order)

}