package com.example.marcin.picturesswipeapp.ui.RemindPasswordView

import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import io.reactivex.Observable

class RemindPasswordViewPresenter(private val networkProvider: NetworkProvider,
                                  private val firebaseAuthProvider: FirebaseAuthProvider,
                                  private val view: RemindPasswordViewContracts.RemindPasswordView)
    : RemindPasswordViewContracts.RemindPasswordViewPresenter, FirebaseAuthProvider.OnFirebaseProvider{

    override fun onViewCreated(emailObserver: Observable<CharSequence>) {
        firebaseAuthProvider.presenter = this
    }

    override fun onRemindButtonTap(email: String) =
            when(networkProvider.isNetworkAvailable()){
                true -> remindPassword(email)
                false -> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
            }

    override fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String) {
        if(action == Consts.REMIND_PASSWORD_ACTION){
            onRemindPasswordActionResult(isSuccess, msg)
        }
    }

    private fun remindPassword(email: String) =
            when(email.isNotBlank()){
                true -> {
                    view.setVisibilityRemindButton(false)
                    view.setVisibilityRemindProgressBar(true)
                    firebaseAuthProvider.remindPassword(email)
                }
                false-> view.showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
            }

    private fun onRemindPasswordActionResult(isSuccess: Boolean, msg: String) {
        when (isSuccess) {
            true -> {
                view.showToast(Strings.WE_SEND_YOUR_PASSWORD)
                view.startLoginActivity()
            }
            false -> view.showToast(msg)
        }
        view.setVisibilityRemindProgressBar(false)
        view.setVisibilityRemindButton(true)
    }

    /*private fun emailObserv(emailObserver: Observable<CharSequence>){
        val validator = InputValidator()
        view.lockRemindButton()
        emailObserver.debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .map { it.toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    when(validator.emailValidation(it).isEmpty()){
                        true -> {
                            view.unlockRemindButton()
                            view.setEmailWrapperError(Strings.EMPTY_STRING)
                        }
                        false -> {
                            view.setEmailWrapperError(validator.emailValidation(it))
                            view.lockRemindButton()
                        }
                    }
                }
    }*/
}