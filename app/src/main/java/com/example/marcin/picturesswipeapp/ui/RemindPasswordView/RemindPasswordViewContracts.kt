package com.example.marcin.picturesswipeapp.ui.RemindPasswordView

import io.reactivex.Observable

interface RemindPasswordViewContracts {

    interface RemindPasswordView{

        fun setEmailWrapperError(msg: String)
        fun showToast(msg: String)
        fun startLoginActivity()
        fun setVisibilityRemindButton(isVisible: Boolean)
        fun setVisibilityRemindProgressBar(isVisible: Boolean)

    }

    interface RemindPasswordViewPresenter{

        fun onViewCreated(emailObserver: Observable<CharSequence>)
        fun onRemindButtonTap(email: String)
    }
}