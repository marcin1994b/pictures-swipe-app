package com.example.marcin.picturesswipeapp.ui.InfoView

import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.example.marcin.picturesswipeapp.Model.ImageData.UrlsData
import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.DownloadFileProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.Model.Providers.PackageManagerProvider
import com.example.marcin.picturesswipeapp.Model.Providers.PermissionsProvider
import com.example.marcin.picturesswipeapp.ui.FavImgListAdapterHolder
import com.example.marcin.picturesswipeapp.ui.SwipeStackViewHolder


class InfoViewPresenter(private val view: InfoViewContracts.InfoView,
                        private val networkProvider: NetworkProvider,
                        private val permissionsProvider: PermissionsProvider,
                        private val downloadFileProvider: DownloadFileProvider,
                        private val packageManagerProvider: PackageManagerProvider)
    : InfoViewContracts.InfoViewPresenter {

    private lateinit var imgData : ImageData

    private var infoViewMode = true
    private var imgViewMode = false

    override fun onViewCreated(imgPositionOnStack: Int, isFromApi: Boolean) =
            when(isFromApi){
                true -> handleImgFromListAdapter(imgPositionOnStack)
                false -> handleImgFromSwipeStackAdapter(imgPositionOnStack)
            }

    override fun onUserNameTap() =
            when(packageManagerProvider.canHandleViewActionIntent()) {
                true -> view.startWebsite(imgData.user.links.html)
                false-> view.showToast("UPS!")
            }

    override fun onUserProfileImgTap() =
            when(packageManagerProvider.canHandleViewActionIntent()) {
                true -> view.startWebsite(imgData.user.links.html)
                false-> view.showToast("UPS!")
            }

    override fun onLinkTap() =
            when(packageManagerProvider.canHandleViewActionIntent()){
                true -> view.startWebsite(imgData.links.html)
                false-> view.showToast("UPS")
            }

    override fun onArrowViewTap(viewOrientation : Int) =
            when(true){
                infoViewMode -> turnOnImgViewMode(viewOrientation)
                imgViewMode -> turnOnInfoViewMode(viewOrientation)
                else -> {}
            }

    override fun onDownloadMenuButtonTap()  =
            when(permissionsProvider.hasExternalStoragePermission()){
                true -> {
                    when(networkProvider.isNetworkAvailable()){
                        true -> downloadFile()
                        false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
                    }
                }
                false-> askWriteExternalStoragePermission()
            }

    override fun onShareMenuButtonTap() =
            when(packageManagerProvider.canHandleSendActionIntent()) {
                true -> {
                    when(networkProvider.isNetworkAvailable()){
                        true -> view.startShareIntent(imgData.links.html)
                        false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
                    }
                }
                false-> view.showToast("UPS")
            }

    override fun onBackArrowMenuButtonTap() {
        view.finishActivity()
    }

    private fun handleImgFromListAdapter(imgPositionOnStack: Int){
        if(imgPositionOnStack != -1){
            FavImgListAdapterHolder.adapter?.let{
                imgData = it.imagesList[imgPositionOnStack]
                setViewsInView()
            }
        }
    }

    private fun handleImgFromSwipeStackAdapter(imgPositionOnStack: Int){
        if(imgPositionOnStack != -1){
            SwipeStackViewHolder.swipeStackAdapter?.let {
                imgData = it.imgList[imgPositionOnStack]
                setViewsInView()
            }
        }
    }

    private fun turnOnImgViewMode(viewOrientation : Int){
        transitionToImageViewMode()
        when(viewOrientation){
            Consts.ORIENTATION_PORTRAIT -> { view.setUpArrow() }
            Consts.ORIENTATION_LANDSCAPE -> { view.setLeftArrow() }
        }
        view.hideToolbar()
    }

    private fun turnOnInfoViewMode(viewOrientation : Int){
        transitionToInfoViewMode()
        when(viewOrientation){
            Consts.ORIENTATION_PORTRAIT -> { view.setDownArrow() }
            Consts.ORIENTATION_LANDSCAPE -> { view.setRightArrow() }
        }
        view.showToolbar()
    }

    private fun setViewsInView(){
        view.setImageView(getImage(AppConfig.stackImagesQuality, imgData.urls),
                getImage(AppConfig.fullImagesQuality, imgData.urls))
        view.setAuthorProfileImageView(imgData.user.profile_image.medium)
        view.setAuthorNameTextView(imgData.user.name)
        view.setUserNameTextView(userNameFormatting(imgData.user.username))
        view.setDateTextView(dateFormatting(imgData.created_at))
        view.setSizeTextView(sizeFormatting(imgData.width.toString(), imgData.height.toString()))
        view.setLinkTestView(Strings.HTTP_UNSPLASH_LINK)
    }

    private fun getImage(config: Int, urls: UrlsData): String{
        return when(config){
            AppConfig.SMALL_IMAGES_QUALITY -> urls.thumb
            AppConfig.MEDIUM_IMAGES_QUALITY -> urls.small
            AppConfig.LARGE_IMAGES_QUALITY -> urls.regular
            else -> urls.regular
        }
    }

    private fun userNameFormatting(userName: String): String = "@$userName"

    private fun dateFormatting(date: String): String = date
            .replace('T', ' ')
            .substring(0, 19)

    private fun sizeFormatting(width: String, height: String): String = "$width x $height"

    private fun transitionToImageViewMode(){
        view.transitionToImageViewMode(imgData.urls.regular)
        infoViewMode = false
        imgViewMode = true
    }

    private fun transitionToInfoViewMode(){
        view.transitionToInfoViewMode(imgData.urls.regular)
        infoViewMode = true
        imgViewMode = false
    }

    private fun downloadFile() =
            downloadFileProvider.downloadFile(imgData.links.download, imgData.id + "img")

    private fun askWriteExternalStoragePermission() =
            permissionsProvider.askWriteExternalStoragePermission()


}