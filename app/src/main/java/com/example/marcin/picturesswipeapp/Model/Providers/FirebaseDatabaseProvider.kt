package com.example.marcin.picturesswipeapp.Model.Providers

import com.example.marcin.picturesswipeapp.Model.AccountData
import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.example.marcin.picturesswipeapp.Model.ImagesListData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class FirebaseDatabaseProvider() {

    interface OnFirebaseDatabaseProviderResult{
        fun onFirebaseDatabaseProviderGetResult(isSuccess: Boolean, any: Any?, msg: String)
    }


    lateinit var presenter: FirebaseDatabaseProvider.OnFirebaseDatabaseProviderResult
    private val reference : DatabaseReference = FirebaseDatabase.getInstance().reference

    fun postUserAccountData(accountData: AccountData){
        reference.child("users")
                .child(getCurrentUser()?.uid)
                .setValue(accountData)
    }

    fun postImage(img: ImageData, addedImagesCounter: Int){
        reference.child("images_lists")
                .child(getCurrentUser()?.uid)
                .child((addedImagesCounter/30).toString())
                .child("imagesList")
                .push()
                .setValue(img)
    }

    fun clearImagesList(){
        reference.child("images_lists")
                .child(getCurrentUser()?.uid)
                .setValue("")
    }

    fun updateAccountEmailData(email: String){
        reference.child("users")
                .child(getCurrentUser()?.uid)
                .updateChildren(emailDataToHashMap(email))
    }

    fun updateAccountImageCounterData(imageCounter: Int){
        reference.child("users")
                .child(getCurrentUser()?.uid)
                .updateChildren(imgCounterDataToHashMap(imageCounter))
    }

    fun updateAccountLikedImagesCounterData(likedImagesCounter: Int){
        reference.child("users")
                .child(getCurrentUser()?.uid)
                .updateChildren(likedImagesCounterDataToHashMap(likedImagesCounter))
    }

    fun getAccountData(){
        reference.child("users")
                .child(getCurrentUser()?.uid)
                .addListenerForSingleValueEvent(valueEventListenerForAccountData())
    }

    fun getImagesList(page: Int){
        val query = reference.child("images_lists")
                .child(getCurrentUser()?.uid)
                .child(page.toString())
                .child("imagesList")
                .orderByChild("added_at")
        query.addListenerForSingleValueEvent(valueEventListenerForImagesList())

    }


    private fun emailDataToHashMap(email: String): HashMap<String, Any>{
        val result = HashMap<String, Any>()
        result.put("email", email)
        return result
    }

    private fun imgCounterDataToHashMap(imageCounter: Int): HashMap<String, Any>{
        val result = HashMap<String, Any>()
        result.put("imagesCounter", imageCounter)
        return result
    }

    private fun likedImagesCounterDataToHashMap(likedImagesCounter: Int) : HashMap<String, Any>{
        val result = HashMap<String, Any>()
        result.put("likedImagesCounter", likedImagesCounter)
        return result
    }

    private fun getCurrentUser(): FirebaseUser?{
        return FirebaseAuth.getInstance().currentUser
    }

    private fun valueEventListenerForAccountData() : ValueEventListener = object : ValueEventListener {
        override fun onCancelled(databaseError: DatabaseError?) {
            presenter.onFirebaseDatabaseProviderGetResult(true, null,
                    databaseError!!.message)
        }

        override fun onDataChange(dataSnapshot: DataSnapshot?) {
            presenter.onFirebaseDatabaseProviderGetResult(true,
                    dataSnapshot?.getValue(AccountData::class.java), "")
        }
    }

    private fun valueEventListenerForImagesList() : ValueEventListener = object : ValueEventListener {
        override fun onCancelled(databaseError: DatabaseError?) {
            presenter.onFirebaseDatabaseProviderGetResult(true, null,
                    databaseError!!.message)
        }

        override fun onDataChange(dataSnapshot: DataSnapshot?) {
            val imagesList = ImagesListData()
            dataSnapshot?.children?.forEach { child ->
                println(child.value.toString())
                val imageData = child.getValue(ImageData::class.java)
                imageData?.let{ img->
                    imagesList.imagesList.put(child.key, img)
                }
            }
            presenter.onFirebaseDatabaseProviderGetResult(true,
                    imagesList, "")
        }
    }

}