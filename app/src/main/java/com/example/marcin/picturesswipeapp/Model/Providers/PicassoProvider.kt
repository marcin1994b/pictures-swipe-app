package com.example.marcin.picturesswipeapp.Model.Providers

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class PicassoProvider(private val context: Context) {

    fun setImgViewInInfoActivity(url: String, imgView: ImageView){
        Picasso.with(context)
                .load(url)
                .centerCrop()
                .fit()
                .into(imgView)
    }

    fun setImgFullView(url: String, imgView: ImageView){
        Picasso.with(context)
                .load(url)
                .into(imgView)
    }

    fun setCardViewImg(url: String, mainView: ImageView, infoView: ImageView){
        Picasso.with(context)
                .load(url)
                .centerCrop()
                .fit()
                .into(mainView, object : Callback{
                    override fun onSuccess() {
                        setInfoImg(infoView)
                    }

                    override fun onError() {}

                })
    }

    private fun setInfoImg(view: ImageView){
        Picasso.with(context)
                .load(android.R.drawable.ic_menu_info_details)
                .centerCrop()
                .fit()
                .into(view)
    }
}