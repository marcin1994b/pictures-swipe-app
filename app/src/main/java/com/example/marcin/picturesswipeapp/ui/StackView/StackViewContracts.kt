package com.example.marcin.picturesswipeapp.ui.StackView

interface StackViewContracts {

    interface MainViewPresenter{

        fun onViewCreated()
        fun onSwipedToLeft(position: Int)
        fun onSwipedToRight(position: Int)
        fun onEmptyStack()
        fun onSwipeStart(position: Int)
        fun onSwipeEnd(position: Int)
        fun onSwipeProgress(position: Int, progress: Float)
    }

    interface MainView {

        fun swipeTopViewToLeft()
        fun swipeTopViewToRight()
        fun startInfoActivity()
        fun showToast(msg: String)
        fun showProgressBar()
        fun hideProgressBar()
        fun undoSwipe()

    }
}