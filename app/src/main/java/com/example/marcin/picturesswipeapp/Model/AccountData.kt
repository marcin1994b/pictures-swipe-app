package com.example.marcin.picturesswipeapp.Model

class AccountData(){

    constructor(email: String, imagesCounter: Int, likedImagesCounter: Int): this(){
        this.email = email
        this.imagesCounter = imagesCounter
        this.likedImagesCounter = likedImagesCounter
    }

    var email: String = ""
    var imagesCounter: Int = 0
    var likedImagesCounter : Int = 0
}