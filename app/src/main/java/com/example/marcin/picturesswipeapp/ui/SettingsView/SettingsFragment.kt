package com.example.marcin.picturesswipeapp.ui.SettingsView

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider
import com.example.marcin.picturesswipeapp.R
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.settingscardview.view.*


class SettingsFragment : Fragment(), SettingsViewContracts.SettingsView {

    var presenter: SettingsViewContracts.SettingsViewPresenter? = null
    private lateinit var layout : View

    companion object {
        fun newInstance(): Fragment = SettingsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater!!.inflate(R.layout.fragment_settings, container, false)
        initPresenter()
        presenter?.onViewCreated()
        return layout
    }

    override fun showToast(msg: String) =
            Toast.makeText(this.activity.applicationContext, msg, Toast.LENGTH_SHORT).show()

    override fun setStackImagesQualityCardView(checked: Int) =
            layout.stack_img_resolution.let{
                it.textview.text = "Stack images quality:"
                it.radioButton1.text = Strings.SMALL
                it.radioButton2.text = Strings.MEDIUM
                it.radioButton3.text = Strings.LARGE
                it.radiogroup.setOnCheckedChangeListener { radioGroup, i ->
                    onRadioGroupClicked(radioGroup, Consts.STACK_IMG_QUALITY_CARDVIEW) }
                setCheckedRadioButton(it.radiogroup, checked)
            }

    override fun setFullImageQualityCardView(checked: Int) =
            layout.full_img_resolution.let{
                it.textview.text = "Full images quality:"
                it.radioButton1.text = Strings.SMALL
                it.radioButton2.text = Strings.MEDIUM
                it.radioButton3.text = Strings.LARGE
                it.radiogroup.setOnCheckedChangeListener { radioGroup, i ->
                    onRadioGroupClicked(radioGroup, Consts.FULL_IMG_QUALITY_CARDVIEW) }
                setCheckedRadioButton(it.radiogroup, checked)
            }

    override fun setMaxSizeOfStackCardView(checked: Int) =
            layout.max_stack_size.let{
                it.textview.text = "Max stack size:"
                it.radioButton1.text = AppConfig.SMALL_MAX_STACK_SIZE.toString()
                it.radioButton2.text = AppConfig.MEDIUM_MAX_STACK_SIZE.toString()
                it.radioButton3.text = AppConfig.LARGE_MAX_STACK_SIZE.toString()
                it.radiogroup.setOnCheckedChangeListener { radioGroup, i ->
                    onRadioGroupClicked(radioGroup, Consts.MAX_STACK_SIZE_CARDVIEW) }
                setCheckedRadioButton(it.radiogroup, checked)
            }

    override fun setMinSizeOfStackCardView(checked: Int) =
            layout.min_stack_size.let{
                it.textview.text = "Min stack size:"
                it.radioButton1.text = AppConfig.SMALL_MIN_STACK_SIZE.toString()
                it.radioButton2.text = AppConfig.MEDIUM_MIN_STACK_SIZE.toString()
                it.radioButton3.text = AppConfig.LARGE_MIN_STACK_SIZE.toString()
                it.radiogroup.setOnCheckedChangeListener { radioGroup, i ->
                    onRadioGroupClicked(radioGroup, Consts.MIN_STACK_SIZE_CARDVIEW) }
                setCheckedRadioButton(it.radiogroup, checked)
            }

    private fun initPresenter(){
        if(presenter == null){
            presenter = SettingsViewPresenter(SharedPreferencesProvider(this.activity.applicationContext),
                    this)
        }

    }

    private fun onRadioGroupClicked(radioGroup: RadioGroup, cardView: Int) =
            when(radioGroup.checkedRadioButtonId){
                radioGroup.radioButton1.id -> presenter?.onRadioGroupClicked(1, cardView)
                radioGroup.radioButton2.id -> presenter?.onRadioGroupClicked(2, cardView)
                radioGroup.radioButton3.id -> presenter?.onRadioGroupClicked(3, cardView)
                else -> {}
            }

    private fun setCheckedRadioButton(radioGroup: RadioGroup, checkedRadioButton: Int) =
            when(checkedRadioButton){
                1 -> radioGroup.radioButton1.isChecked = true
                2 -> radioGroup.radioButton2.isChecked = true
                3 -> radioGroup.radioButton3.isChecked = true
                else -> {}
            }
}