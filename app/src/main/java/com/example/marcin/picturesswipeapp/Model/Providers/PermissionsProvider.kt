package com.example.marcin.picturesswipeapp.Model.Providers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.BasePermissionListener

class PermissionsProvider(private val activity: Activity,
                          private val context: Context) {

    fun askWriteExternalStoragePermission(){
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(BasePermissionListener())
                .onSameThread()
                .check()
    }

    fun hasExternalStoragePermission(): Boolean =
            activity.packageManager.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    context.packageName) == PackageManager.PERMISSION_GRANTED

}