package com.example.marcin.picturesswipeapp.Model

import com.example.marcin.picturesswipeapp.Model.Objects.Strings


class InputValidator() {

    fun emailValidation(email: String): String{
        if(email.contains('@', true)){
            val substringBefore = email.substring(0,email.indexOf('@', 0, true))
            if(substringBefore.length >= 3){
                val substringBehind = email.substring(email.indexOf('@', 0, true), email.length)
                if(substringBehind.length >= 3){
                    if(substringBehind.contains('.')){
                        val substringBehindDot = email.substring(email.indexOf('.', 0, true)+1, email.length)
                        if(substringBehindDot.length >=2){
                            return Strings.EMAIL_GOOD
                        }else{
                            return Strings.EMAIL_TOO_SHORT_BEHIND_DOT
                        }
                    }else{
                        return Strings.EMAIL_NOT_CONTAINS_DOT
                    }
                }else{
                    return Strings.EMAIL_SEQUENCE_BEHIND_TOO_SHORT
                }
            }else{
                return Strings.EMAIL_SEQUENCE_BEFORE_TOO_SHORT
            }
        }else{
            return Strings.EMAIL_NOT_CONTAINS
        }
    }

    fun passwordValidation(password: String): String{
        if(password.length >= 6){
            if(password.any { it.isLetter() }){
                if(password.any { it.isDigit() }){
                    return Strings.PASSWORD_GOOD
                }else{
                    return Strings.PASSWORD_MUST_HAVE_DIGIT
                }
            }else{
                return Strings.PASSWORD_MUST_HAVE_LETTER
            }
        }else{
            return Strings.PASSWORD_TOO_SHORT
        }
    }

    fun arePasswordsTheSame(first: String, second: String) : Boolean = first == second
}