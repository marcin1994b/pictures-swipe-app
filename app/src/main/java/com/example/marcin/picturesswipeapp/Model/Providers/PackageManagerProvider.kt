package com.example.marcin.picturesswipeapp.Model.Providers

import android.content.Context
import android.content.Intent

/**
 * Created by Marcin on 21.10.2017.
 */
class PackageManagerProvider(private val context: Context) {

    fun canHandleViewActionIntent(): Boolean{
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        if(intent.resolveActivity(context.packageManager) != null){
            return true
        }
        return false
    }

    fun canHandleSendActionIntent(): Boolean{
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        if(intent.resolveActivity(context.packageManager) != null){
            return true
        }
        return false
    }
}