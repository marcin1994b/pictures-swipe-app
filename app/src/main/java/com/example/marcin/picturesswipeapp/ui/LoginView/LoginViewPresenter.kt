package com.example.marcin.picturesswipeapp.ui.LoginView

import android.util.Log
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider

class LoginViewPresenter(private val networkProvider: NetworkProvider,
                         private val firebaseAuthProvider: FirebaseAuthProvider,
                         private val view: LoginViewContracts.LoginView)
    : LoginViewContracts.LoginViewPresenter, FirebaseAuthProvider.OnFirebaseProvider {

    override fun onLoginButtonTap(email: String, password: String) {
        Log.d("LoginViewPresenter", "on login button tap")
        firebaseAuthProvider.presenter = this
        when(!email.isBlank() && !password.isBlank()){
            true -> login(email, password)
            false-> view.showToast(Strings.INPUT_EMAIL_AND_PASSWORD)
        }
    }

    override fun onNotRememberTextViewTap() {
        view.startRemindPasswordActivity()
    }

    override fun onCreateAccountTextViewTap() {
        view.startCreateAccountActivity()
    }

    override fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String) {
        if(action == Consts.SIGN_IN_USER_ACCTION){
            when(isSuccess){
                true -> view.startMainActivity()
                false -> view.showToast(msg)
            }
        }
        view.setVisibilityProgressBar(false)
        view.setVisibilityLoginButton(true)
    }

    private fun login(email: String, password: String) =
            when(networkProvider.isNetworkAvailable()) {
                true ->{
                    view.setVisibilityLoginButton(false)
                    view.setVisibilityProgressBar(true)
                    firebaseAuthProvider.signIn(email, password)
                }
                false-> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
            }

}