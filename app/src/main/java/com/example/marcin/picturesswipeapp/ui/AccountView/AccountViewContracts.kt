package com.example.marcin.picturesswipeapp.ui.AccountView

import io.reactivex.Observable

interface AccountViewContracts {

    interface AccountView{

        fun expandVerifyView()
        fun expandChangeEmailView()
        fun expandChangePasswordView()
        fun expandRestartAccountView()
        fun expandRemoveAccountView()

        fun collapseVerifyView()
        fun collapseChangeEmailView()
        fun collapseChangePasswordView()
        fun collapseRestartAccountView()
        fun collapseRemoveAccountView()

        fun showToast(msg: String)
        fun setVerifyView(isEmailVerified: Boolean)
        fun setRemoveAccountView()

        fun setEnabledVerifyEmailButton(isEnable: Boolean)
        fun setEnabledChangeEmailButton(isEnable: Boolean)
        fun setEnabledChangePasswordButton(isEnable: Boolean)
        fun setEnabledRestartAccountButton(isEnable: Boolean)
        fun setEnabledRemoveAccountButton(isEnable: Boolean)

        fun setVisibilityVerifyButton(isVisible: Boolean)
        fun setVisibilityChangeEmailButton(isVisible: Boolean)
        fun setVisibilityChangePasswordButton(isVisible: Boolean)
        fun setVisibilityRestartAccountButton(isVisible: Boolean)
        fun setVisibilityRemoveAccountButton(isVisible: Boolean)

        fun setVisibilityVerifyProgressBar(isVisible: Boolean)
        fun setVisibilityChangeEmailProgressBar(isVisible: Boolean)
        fun setVisibilityChangePasswordProgressBar(isVisible: Boolean)
        fun setVisibilityRestartAccountProgressBar(isVisible: Boolean)
        fun setVisibilityRemoveAccountProgressBar(isVisible: Boolean)

        fun setNewEmailWrapperErrorInChangeEmailView(error: String)
        fun setFirstNewPassWrapperErrorInChangePasswordView(error: String)
        fun setSecondNewPassWrapperErrorInChangePasswordView(error: String)

        fun startSignInActivity()
    }

    interface AccountViewPresenter{

        fun onViewCreated()

        fun onVerifyViewArrowTap()
        fun onChangeEmailViewArrowTap()
        fun onChangePasswordViewArrowTap()
        fun onRestartAccountViewArrowTap()
        fun onRemoveAccountViewArrowTap()

        fun onVerifyViewButtonTap()
        fun onChangeEmailViewButtonTap(newEmail: String, password: String)
        fun onChangePasswordViewButtonTap(currentPass: String, newPassword: String)
        fun onRestartAccountViewButtonTap(currentPass: String)
        fun onRemoveAccountViewButtonTap(currentPass: String)

        fun observChangeEmailView(newEmailObserver: Observable<CharSequence>,
                                          currentPassObserver: Observable<CharSequence>)
        fun observChangePasswordView(currentPassword: Observable<CharSequence>,
                                     firstPass: Observable<CharSequence>, secondPass: Observable<CharSequence>)
        fun observRestartAccountView(currentPass: Observable<CharSequence>)
        fun observRemoveAccountView(currentPass: Observable<CharSequence>)
    }
}