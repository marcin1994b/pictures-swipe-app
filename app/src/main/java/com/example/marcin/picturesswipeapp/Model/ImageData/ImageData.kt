package com.example.marcin.picturesswipeapp.Model.ImageData

class ImageData(){

    constructor( id: String, created_at: String, width: Int, height: Int, color: String,
                 urls: UrlsData, user: UserData, links: Links) : this() {

        this.added_at = ""
        this.created_at = created_at
        this.id = id
        this.width = width
        this.height = height
        this.color = color
        this.urls = urls
        this.user = user
        this.links = links
    }


    var added_at: String = ""
    var id: String = ""
    var created_at: String = ""
    var width: Int = 0
    var height: Int = 0
    var color: String = ""
    var urls: UrlsData =  UrlsData()
    var user: UserData = UserData()
    var links: Links = Links()
}


