package com.example.marcin.picturesswipeapp.Model.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.example.marcin.picturesswipeapp.Model.ImageData.UrlsData
import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Providers.PicassoProvider
import com.example.marcin.picturesswipeapp.R
import kotlinx.android.synthetic.main.image_card_item_constraint.view.*


class SwipeStackAdapter(private val context: Context) : BaseAdapter() {

    var imgList: MutableList<ImageData> = mutableListOf()

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val picassoProvider = PicassoProvider(context)
        val convertView = inflater.inflate(R.layout.image_card_item_constraint, viewGroup, false)
        picassoProvider.setCardViewImg(getImgUrl(imgList[position].urls), convertView.imageView5, convertView.imageView4)
        return convertView
    }

    override fun getItem(position: Int): ImageData = imgList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = imgList.size

    fun clearList(){
        imgList.clear()
    }

    fun addItem(imgData: ImageData){
        imgList.add(imgData)
        this.notifyDataSetChanged()
    }

    fun addItems(list: MutableList<ImageData>){
        imgList.addAll(list)
        this.notifyDataSetChanged()
    }

    private fun getImgUrl(urls: UrlsData): String =
        when(AppConfig.stackImagesQuality){
            AppConfig.SMALL_IMAGES_QUALITY -> urls.thumb
            AppConfig.MEDIUM_IMAGES_QUALITY -> urls.small
            AppConfig.LARGE_IMAGES_QUALITY -> urls.regular
            else -> urls.regular
    }
}