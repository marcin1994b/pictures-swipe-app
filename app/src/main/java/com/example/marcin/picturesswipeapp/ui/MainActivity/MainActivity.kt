package com.example.marcin.picturesswipeapp.ui.MainActivity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.AccountView.AccountFragment
import com.example.marcin.picturesswipeapp.ui.LikedImagesView.LikedImagesFragment
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.example.marcin.picturesswipeapp.ui.SettingsView.SettingsFragment
import com.example.marcin.picturesswipeapp.ui.StackView.StackFragment
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.app_bar_main2.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        MainViewContracts.MainView {

     var presenter: MainViewContracts.MainViewPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar)
        initNavDrawer()
        initPresenter()
        presenter?.onViewCreated()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_stack_view_item -> presenter?.onNavStackViewItemTap()
            R.id.nav_recycler_view_item -> presenter?.onNavLikedImagesViewItemTap()
            R.id.nav_account_view_item -> presenter?.onNavAccountViewItemTap()
            R.id.nav_settings_view_item -> presenter?.onNavSettingsViewItemTap()
            R.id.nav_sign_out_item -> presenter?.onNavSignOutItemTap()
        }
        return true
    }

    override fun closeNavDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    override fun startStackView() {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.include_content, StackFragment.newInstance())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.commit()
    }

    override fun startLikedImagesView() {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.include_content, LikedImagesFragment.newInstance())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.commit()
    }

    override fun startAccountView(){
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.include_content, AccountFragment.newInstance())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.commit()
    }

    override fun startSettingsView(){
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.include_content, SettingsFragment.newInstance())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.commit()
    }

    override fun startSignInView() {
        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun setNavDrawerTextView(text: String) {
        val headerView = nav_view.inflateHeaderView(R.layout.nav_header_main2)
        val textView = headerView.findViewById<TextView>(R.id.nav_drawer_textView)
        textView.text = text
    }

    private fun initPresenter(){
        if(presenter == null) {
            presenter = MainViewPresenter(NetworkProvider(applicationContext),
                    FirebaseAuthProvider(this), SharedPreferencesProvider(applicationContext),
                    this)
        }
    }

    private fun initNavDrawer(){
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

}
