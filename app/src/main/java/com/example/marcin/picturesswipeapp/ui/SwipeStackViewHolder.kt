package com.example.marcin.picturesswipeapp.ui

import android.annotation.SuppressLint
import com.example.marcin.picturesswipeapp.Model.Adapters.SwipeStackAdapter

/**
 * Created by Marcin on 15.10.2017.
*/
object SwipeStackViewHolder {

    @SuppressLint("StaticFieldLeak")
    var  swipeStackAdapter: SwipeStackAdapter? = null
}