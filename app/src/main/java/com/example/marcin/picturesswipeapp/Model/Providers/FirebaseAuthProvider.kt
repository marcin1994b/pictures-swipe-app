package com.example.marcin.picturesswipeapp.Model.Providers

import android.app.Activity
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth

class FirebaseAuthProvider(private val activity: Activity) {

    val CREATE_NEW_ACCOUNT_ACTION = 1
    val SIGN_IN_USER_ACCTION = 2
    val REMIND_PASSWORD_ACTION = 3
    val SIGN_OUT_USER_ACTION = 4
    val RE_AUTH_USER_ACTION = 5
    val UPDATE_EMAIL_ACTION = 6
    val UPDATE_PASSWORD_ACTION = 7
    val REMOVE_ACCOUNT_ACTION = 8
    val SEND_EMAIL_VERIFICATION_ACTION = 9

    interface OnFirebaseProvider{
        fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String)
    }

    private val firebaseAuth : FirebaseAuth = FirebaseAuth.getInstance()
    lateinit var presenter : OnFirebaseProvider

    fun createNewAccount(email: String, password: String) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    presenter.onFirebaseProviderResult(task.isSuccessful, CREATE_NEW_ACCOUNT_ACTION,
                            task.exception.toString()) }
    }



    fun isUserSignedIn() : Boolean{
        if(firebaseAuth.currentUser != null){
            return true
        }
        return false
    }

    fun signIn(email: String, password: String){
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity) { task ->
                    presenter.onFirebaseProviderResult(task.isSuccessful, SIGN_IN_USER_ACCTION,
                            task.exception.toString()) }
    }

    fun getCurrentUserEmail(): String? {
        firebaseAuth.currentUser?.let{
            return it.email
        }
        return null
    }

    fun sendEmailVerification() {
        firebaseAuth.currentUser?.sendEmailVerification()!!.addOnCompleteListener {
            presenter.onFirebaseProviderResult(it.isSuccessful, SEND_EMAIL_VERIFICATION_ACTION,
                    it.exception.toString())

        }
    }

    fun isUserEmailVerified(): Boolean{
        return firebaseAuth.currentUser!!.isEmailVerified
    }

    fun remindPassword(email: String){
        firebaseAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(activity, {task ->
                    presenter.onFirebaseProviderResult(task.isSuccessful, REMIND_PASSWORD_ACTION,
                            task.exception.toString())
                })
    }

    fun signOut() =
            when(firebaseAuth.currentUser != null) {
                true -> signOutUser()
                false -> signOutUserFailed()
            }

    fun updateEmail(newEmail: String, currentPassword: String) {
        firebaseAuth.currentUser?.reauthenticate(getUserCredential(currentPassword))!!
                .addOnCompleteListener {
                    when (it.isSuccessful) {
                        true -> updateUserEmail(newEmail)
                        false -> reAuthProcessFailed(it, UPDATE_EMAIL_ACTION)
                    }
                }
    }

    fun updatePassword(currentPassword: String, newPassword: String) {
        firebaseAuth.currentUser?.reauthenticate(getUserCredential(currentPassword))!!
                .addOnCompleteListener {
                    when (it.isSuccessful) {
                        true -> updateUserPassword(newPassword)
                        false -> reAuthProcessFailed(it, UPDATE_PASSWORD_ACTION)
                    }
                }
    }


    fun removeAccount(currentPassword: String) {
        firebaseAuth.currentUser?.reauthenticate(getUserCredential(currentPassword))!!
                .addOnCompleteListener {
                    when (it.isSuccessful) {
                        true -> removeUserAccount()
                        false -> reAuthProcessFailed(it, REMOVE_ACCOUNT_ACTION)
                    }
                }
    }

    fun reSignInUser(password: String) {
        firebaseAuth.currentUser?.reauthenticate(getUserCredential(password))!!
                .addOnCompleteListener {
                    presenter.onFirebaseProviderResult(it.isSuccessful,
                            RE_AUTH_USER_ACTION, it.exception.toString())
                }
    }

    private fun getUserCredential(password: String): AuthCredential{
        var currentEmail = ""
        firebaseAuth.currentUser?.let{
            currentEmail = it.email!!
        }
        return EmailAuthProvider.getCredential(currentEmail, password)
    }

    private fun signOutUser(){
        firebaseAuth.signOut()
        presenter.onFirebaseProviderResult(true, SIGN_OUT_USER_ACTION, Strings.EMPTY_STRING)
    }

    private fun updateUserEmail(email: String) {
        firebaseAuth.currentUser!!.updateEmail(email).addOnCompleteListener {
            presenter.onFirebaseProviderResult(it.isSuccessful,
                    UPDATE_EMAIL_ACTION, it.exception.toString())
        }
    }

    private fun updateUserPassword(password: String) {
        firebaseAuth.currentUser!!.updatePassword(password).addOnCompleteListener {
            presenter.onFirebaseProviderResult(it.isSuccessful,
                    UPDATE_PASSWORD_ACTION, it.exception.toString())
        }
    }

    private fun removeUserAccount() {
        firebaseAuth.currentUser!!.delete().addOnCompleteListener {
            presenter.onFirebaseProviderResult(it.isSuccessful,
                    REMOVE_ACCOUNT_ACTION, it.exception.toString())
        }
    }

    private fun signOutUserFailed() {
        presenter.onFirebaseProviderResult(false, SIGN_OUT_USER_ACTION, "UPS!")
    }


    private fun reAuthProcessFailed(task: Task<Void>, actionId: Int) {
        presenter.onFirebaseProviderResult(false,
                actionId, task.exception.toString())
    }


}