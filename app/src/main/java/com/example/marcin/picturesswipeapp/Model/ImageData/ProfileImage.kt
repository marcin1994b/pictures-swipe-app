package com.example.marcin.picturesswipeapp.Model.ImageData

class ProfileImage() {

    constructor(small: String, medium: String, large: String) : this() {
        this.small = small
        this.medium = medium
        this.large = large
    }

    var small: String = ""
    var medium: String = ""
    var large: String = ""
}
