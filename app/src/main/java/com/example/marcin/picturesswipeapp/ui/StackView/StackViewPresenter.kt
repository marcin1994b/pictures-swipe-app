package com.example.marcin.picturesswipeapp.ui.StackView

import android.util.Log
import com.example.marcin.picturesswipeapp.Model.AccountData
import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.ApiProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.ui.SwipeStackViewHolder
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class StackViewPresenter(private val networkProvider: NetworkProvider,
                         private val apiProvider: ApiProvider,
                         private val view: StackViewContracts.MainView,
                         private val firebaseDatabaseProvider: FirebaseDatabaseProvider)
    : StackViewContracts.MainViewPresenter, FirebaseDatabaseProvider.OnFirebaseDatabaseProviderResult {

    private var IMG_PER_PAGE = 20
    private val IMG_ORDER = "popular"

    private var swipeProgress : Float = 0.0F
    private var pageCounter = 0
    private var imagesToRemoveFromStack = 0
    private var isDownloading = false

    private lateinit var accountData : AccountData

    override fun onViewCreated() {
        IMG_PER_PAGE = AppConfig.maxStackSize
        initFirebaseProviders()
        firebaseDatabaseProvider.getAccountData()
    }

    override fun onSwipedToLeft(position: Int) {
        Log.d("StackViewPresenter", "on swipeed to left")
        when(networkProvider.isNetworkAvailable()){
            true -> handleLeftSwipe(position)
            false-> {
                view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
                view.undoSwipe()
            }
        }
    }

    override fun onSwipedToRight(position: Int) {
        Log.d("StackViewPresenter", "on swiped to right")
        when(networkProvider.isNetworkAvailable()){
            true -> handleRightSwipe(position)
            false-> {
                view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
                view.undoSwipe()
            }
        }
    }

    private fun handleRightSwipe(position: Int){
        accountData.imagesCounter++
        accountData.likedImagesCounter++
        val img = SwipeStackViewHolder.swipeStackAdapter!!.getItem(position)
        img.added_at = (System.currentTimeMillis()/1000).toString()
        firebaseDatabaseProvider.updateAccountImageCounterData(accountData.imagesCounter)
        firebaseDatabaseProvider.updateAccountLikedImagesCounterData(accountData.likedImagesCounter)
        firebaseDatabaseProvider.postImage(img, accountData.likedImagesCounter-1)
        if(shouldDownloadNextPage(position)){
            loadNextImages()
        }
    }

    private fun handleLeftSwipe(position: Int){
        accountData.imagesCounter++
        firebaseDatabaseProvider.updateAccountImageCounterData(accountData.imagesCounter)
        if(shouldDownloadNextPage(position)){
            loadNextImages()
        }
    }

    override fun onEmptyStack() {
        Log.d("StackViewPresenter", "on stack empty")
    }

    override fun onSwipeStart(position: Int) {
//        Log.d("StackViewPresenter", "on swipe start")
    }

    override fun onSwipeEnd(position: Int) {
        if(isSwipeProgressSmallEnoughToStartActivity()){
            Log.d("StackViewPresenter", "Start info activity")
            view.startInfoActivity()
        }
        swipeProgress = 0.0F

    }

    override fun onSwipeProgress(position: Int, progress: Float) {
        swipeProgress = progress
    }

    override fun onFirebaseDatabaseProviderGetResult(isSuccess: Boolean, any: Any?, msg: String) {
        when(isSuccess){
            true -> {
                any?.let{
                    initAccountData(it as AccountData)
                    calculatePageCounter()
                    calculateImagesToRemoveFromStack()
                    println("Pobieram $IMG_PER_PAGE imgs oraz ${pageCounter+1} strone")
                    downloadImagesAndPutToStack(apiProvider.getPhotos(IMG_PER_PAGE, pageCounter+1 , IMG_ORDER))
                }
            }
            false -> view.showToast(msg)
        }
    }

    private fun loadNextImages(){
        Log.d("StackViewPresenter", "load next images")
        when(networkProvider.isNetworkAvailable()){
            true -> {
                isDownloading = true
                downloadImagesAndPutToStack(apiProvider.getPhotos(IMG_PER_PAGE, pageCounter+1, IMG_ORDER))
            }
            false -> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    private fun downloadImagesAndPutToStack(single: Single<List<ImageData>>){
        single.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(response: List<ImageData>){
        val list = response.toMutableList()
        println("${list.size}")
        removeWatchedImages(list)
        SwipeStackViewHolder.swipeStackAdapter?.addItems(list)
        pageCounter++
        isDownloading = false
        view.hideProgressBar()
    }

    private fun handleError(error: Throwable){
        Log.d("StackViewPresenter", "ERROR ${error.message}")
        view.showToast(error.message.toString())
        isDownloading = false
    }

    private fun shouldDownloadNextPage(position: Int): Boolean {
        SwipeStackViewHolder.swipeStackAdapter?.let {
            if(it.count - position <= AppConfig.minStackSize && !isDownloading){
                return true
            }
        }
        return false
    }

    private fun isSwipeProgressSmallEnoughToStartActivity(): Boolean =
            (swipeProgress < 0.003 && swipeProgress > -0.003)

    private fun removeWatchedImages(list: MutableList<ImageData>){
        while(imagesToRemoveFromStack != 0){
            list.removeAt(0)
            imagesToRemoveFromStack--
        }
    }

    private fun initAccountData(accountData: AccountData){
        this.accountData = accountData
    }

    private fun initFirebaseProviders(){
        firebaseDatabaseProvider.presenter = this
    }

    private fun calculatePageCounter(){
        pageCounter = (accountData.imagesCounter / IMG_PER_PAGE)
    }

    private fun calculateImagesToRemoveFromStack(){
        imagesToRemoveFromStack = accountData.imagesCounter - pageCounter*IMG_PER_PAGE
    }

}