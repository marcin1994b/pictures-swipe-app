package com.example.marcin.picturesswipeapp.ui.SettingsView

import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider

class SettingsViewPresenter(private val sharedPreferencesProvider: SharedPreferencesProvider,
                            private val view: SettingsViewContracts.SettingsView)
    : SettingsViewContracts.SettingsViewPresenter {

    override fun onViewCreated() {
        getConfigDataFromSharedPreferences()
        setStackImageQualityCardView()
        setFullImageQualityCardView()
        setMaxStackSizeCardView()
        setMinStackSizeCardView()
    }

    override fun onRadioGroupClicked(radioButton: Int, cardView: Int) =
            when(cardView){
                Consts.STACK_IMG_QUALITY_CARDVIEW -> setStackImgQualityConfig(radioButton)
                Consts.FULL_IMG_QUALITY_CARDVIEW -> setFullImageQualityConfig(radioButton)
                Consts.MAX_STACK_SIZE_CARDVIEW -> setMaxStackSizeConfig(radioButton)
                Consts.MIN_STACK_SIZE_CARDVIEW -> setMinStackSizeConfig(radioButton)
                else -> {}
            }

    private fun getConfigDataFromSharedPreferences(){
        AppConfig.stackImagesQuality = sharedPreferencesProvider.restoreData(
                sharedPreferencesProvider.IMAGES_STACK_QUALITY_CONFIG_NAME)
        AppConfig.fullImagesQuality = sharedPreferencesProvider.restoreData(
                sharedPreferencesProvider.FULL_IMAGES_QUALITY_CONFIG_NAME)
        AppConfig.maxStackSize = sharedPreferencesProvider.restoreData(
                sharedPreferencesProvider.MAX_STACK_SIZE_CONFIG_NAME)
        AppConfig.minStackSize = sharedPreferencesProvider.restoreData(
                sharedPreferencesProvider.MIN_STACK_SIZE_CONFIG_NAME)
    }

    private fun setStackImageQualityCardView() =
            when(AppConfig.stackImagesQuality){
                AppConfig.SMALL_IMAGES_QUALITY -> view.setStackImagesQualityCardView(1)
                AppConfig.MEDIUM_IMAGES_QUALITY -> view.setStackImagesQualityCardView(2)
                AppConfig.LARGE_IMAGES_QUALITY -> view.setStackImagesQualityCardView(3)
                else -> view.setStackImagesQualityCardView(1)
            }

    private fun setFullImageQualityCardView() =
            when(AppConfig.fullImagesQuality){
                AppConfig.SMALL_IMAGES_QUALITY -> view.setFullImageQualityCardView(1)
                AppConfig.MEDIUM_IMAGES_QUALITY -> view.setFullImageQualityCardView(2)
                AppConfig.LARGE_IMAGES_QUALITY -> view.setFullImageQualityCardView(3)
                else -> view.setFullImageQualityCardView(1)
            }

    private fun setMaxStackSizeCardView() =
            when(AppConfig.maxStackSize){
                AppConfig.SMALL_MAX_STACK_SIZE -> view.setMaxSizeOfStackCardView(1)
                AppConfig.MEDIUM_MAX_STACK_SIZE -> view.setMaxSizeOfStackCardView(2)
                AppConfig.LARGE_MAX_STACK_SIZE -> view.setMaxSizeOfStackCardView(3)
                else -> view.setMaxSizeOfStackCardView(1)
            }

    private fun setMinStackSizeCardView() =
            when(AppConfig.minStackSize){
                AppConfig.SMALL_MIN_STACK_SIZE -> view.setMinSizeOfStackCardView(1)
                AppConfig.MEDIUM_MIN_STACK_SIZE -> view.setMinSizeOfStackCardView(2)
                AppConfig.LARGE_MIN_STACK_SIZE -> view.setMinSizeOfStackCardView(3)
                else -> view.setMinSizeOfStackCardView(1)
            }

    private fun setStackImgQualityConfig(value: Int){
        when(value){
            1 -> AppConfig.stackImagesQuality = AppConfig.SMALL_IMAGES_QUALITY
            2 -> AppConfig.stackImagesQuality = AppConfig.MEDIUM_IMAGES_QUALITY
            3 -> AppConfig.stackImagesQuality = AppConfig.LARGE_IMAGES_QUALITY
        }
        saveDataToSharedPreferences(Consts.IMAGES_STACK_QUALITY_CONFIG_NAME,
                AppConfig.stackImagesQuality)
    }

    private fun setFullImageQualityConfig(value: Int){
        when(value){
            1 -> AppConfig.fullImagesQuality = AppConfig.SMALL_IMAGES_QUALITY
            2 -> AppConfig.fullImagesQuality = AppConfig.MEDIUM_IMAGES_QUALITY
            3 -> AppConfig.fullImagesQuality = AppConfig.LARGE_IMAGES_QUALITY
        }
        saveDataToSharedPreferences(Consts.FULL_IMAGES_QUALITY_CONFIG_NAME,
                AppConfig.fullImagesQuality)
    }

    private fun setMaxStackSizeConfig(value: Int){
        when(value){
            1 -> AppConfig.maxStackSize = AppConfig.SMALL_MAX_STACK_SIZE
            2 -> AppConfig.maxStackSize = AppConfig.MEDIUM_MAX_STACK_SIZE
            3 -> AppConfig.maxStackSize = AppConfig.LARGE_MAX_STACK_SIZE
        }
        saveDataToSharedPreferences(Consts.MAX_STACK_SIZE_CONFIG_NAME,
                AppConfig.maxStackSize)
    }

    private fun setMinStackSizeConfig(value: Int){
        when(value){
            1 -> AppConfig.minStackSize = AppConfig.SMALL_MIN_STACK_SIZE
            2 -> AppConfig.minStackSize = AppConfig.MEDIUM_MIN_STACK_SIZE
            3 -> AppConfig.minStackSize = AppConfig.LARGE_MIN_STACK_SIZE
        }
        saveDataToSharedPreferences(Consts.MIN_STACK_SIZE_CONFIG_NAME,
                AppConfig.minStackSize)
    }

    private fun saveDataToSharedPreferences(key: String, value: Int) =
            sharedPreferencesProvider.saveData(key, value)


}