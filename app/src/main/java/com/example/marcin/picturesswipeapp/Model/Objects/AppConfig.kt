package com.example.marcin.picturesswipeapp.Model.Objects

object AppConfig {

    val SMALL_IMAGES_QUALITY = 1
    val MEDIUM_IMAGES_QUALITY = 2
    val LARGE_IMAGES_QUALITY = 3

    val SMALL_MAX_STACK_SIZE = 15
    val MEDIUM_MAX_STACK_SIZE = 20
    val LARGE_MAX_STACK_SIZE = 30

    val SMALL_MIN_STACK_SIZE = 5
    val MEDIUM_MIN_STACK_SIZE = 8
    val LARGE_MIN_STACK_SIZE = 10

    var stackImagesQuality = SMALL_IMAGES_QUALITY
    var fullImagesQuality = SMALL_IMAGES_QUALITY
    var maxStackSize = SMALL_MAX_STACK_SIZE
    var minStackSize = SMALL_MIN_STACK_SIZE
}