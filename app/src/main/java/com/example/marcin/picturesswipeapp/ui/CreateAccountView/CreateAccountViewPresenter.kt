package com.example.marcin.picturesswipeapp.ui.CreateAccountView

import android.util.Log
import com.example.marcin.picturesswipeapp.Model.AccountData
import com.example.marcin.picturesswipeapp.Model.InputValidator
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class CreateAccountViewPresenter(private val networkProvider: NetworkProvider,
                                 private val firebaseAuthProvider: FirebaseAuthProvider,
                                 private val firebaseDataProvider: FirebaseDatabaseProvider,
                                 private val view: CreateAccountViewContracts.CreateAccountView)
    : CreateAccountViewContracts.CreateAccountPresenter, FirebaseAuthProvider.OnFirebaseProvider,
FirebaseDatabaseProvider.OnFirebaseDatabaseProviderResult{

    private lateinit var accountData : AccountData

    private val validator: InputValidator = InputValidator()
    //var firebaseDataProvider = FirebaseDatabaseProvider()

    override fun onCreated(emailObservable: Observable<CharSequence>,
                           firstPasswordObservable: Observable<CharSequence>,
                           secondPasswordObservable: Observable<CharSequence>) {
        Log.d("CreateAccountViewPresen", "on created")
        initFirebase()
        observInputs(emailObservable, firstPasswordObservable, secondPasswordObservable)
    }

    override fun onCreateButtonTap(email: String, firstPassword: String, secondPassword: String) {
        Log.d("CreateAccountViewPresen", "on create button tap")
        when(networkProvider.isNetworkAvailable()){
            true -> {
                view.setVisibilityCreateButton(false)
                view.setVisibilityCreateProgressBar(true)
                accountData = AccountData(email, 0,0)
                firebaseAuthProvider.createNewAccount(email, firstPassword)
            }
            false -> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }

    }

    override fun onHaveAccountTextViewTap() {
        Log.d("CreateAccountViewPresen", "on have account text view tap")
        view.startLoginActivity()
    }

    override fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String) {
        if(action == Consts.CREATE_NEW_ACCOUNT_ACTION){
            onCreateAccountActionResult(isSuccess, msg)
        }
    }

    override fun onFirebaseDatabaseProviderGetResult(isSuccess: Boolean, any: Any?, msg: String) {}

    private fun initFirebase(){
        firebaseAuthProvider.presenter = this
        firebaseDataProvider.presenter = this
    }

    private fun observInputs(email: Observable<CharSequence>, firstPass: Observable<CharSequence>,
                             secPass: Observable<CharSequence>){
        Observable.combineLatest(email, firstPass, secPass,
                Function3<CharSequence, CharSequence, CharSequence, MutableList<String>>{
                    e, p1, p2 -> mutableListOf(e.toString(), p1.toString(),
                        p2.toString())
                })
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    val emailValidResult = emailValidation(it[0])
                    val passValidResult = passwordValidation(it[1])
                    val isPasswordsSame = twoPasswordValidation(it[1], it[2])
                    val result = emailValidResult && passValidResult && isPasswordsSame
                    view.setEnabledCreateButton(result)
                }
    }

    private fun emailValidation(email: String): Boolean{
        var isFine = false
        var result = ""
        if(email.isNotBlank()){
            result = validator.emailValidation(email)
            isFine = result.isBlank()
        }
        view.showEmailError(result)
        return isFine
    }

    private fun passwordValidation(pass: String): Boolean{
        var isFine = false
        var result = ""
        if(pass.isNotBlank()){
            result = validator.passwordValidation(pass)
            isFine = result.isBlank()
        }
        view.showFirstPasswordError(result)
        return isFine
    }

    private fun twoPasswordValidation(first: String, second: String): Boolean{
        var result = ""
        if(first != second){
            result = Strings.PASSWORD_ARE_DIFFERENT
        }
        view.showSecondPasswordError(result)
        return result.isBlank()
    }

    private fun onCreateAccountActionResult(isSuccess: Boolean, msg: String) {
        when (isSuccess) {
            true -> {
                firebaseAuthProvider.sendEmailVerification()
                firebaseDataProvider.postUserAccountData(accountData)
                firebaseAuthProvider.signOut()
                view.startLoginActivity()
            }
            false -> view.showToast(msg)
        }
        view.setVisibilityCreateProgressBar(false)
        view.setVisibilityCreateButton(true)
    }

}