package com.example.marcin.picturesswipeapp.ui.LoginView

interface LoginViewContracts {

    interface LoginView{

        fun startCreateAccountActivity()
        fun startRemindPasswordActivity()
        fun startMainActivity()
        fun showToast(msg: String)
        fun setVisibilityLoginButton(isVisible: Boolean)
        fun setVisibilityProgressBar(isVisible: Boolean)
    }

    interface LoginViewPresenter{

        fun onLoginButtonTap(email: String, password: String)
        fun onNotRememberTextViewTap()
        fun onCreateAccountTextViewTap()
    }
}