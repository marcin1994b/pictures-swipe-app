package com.example.marcin.picturesswipeapp.ui.RemindPasswordView

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.activity_remind_password.*

class RemindPasswordActivity : AppCompatActivity(), RemindPasswordViewContracts.RemindPasswordView {

    lateinit var presenter : RemindPasswordViewContracts.RemindPasswordViewPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_remind_password)
        initPresenter()
        initListener()
        presenter.onViewCreated(email_edittext.textChanges())
    }

    override fun setVisibilityRemindButton(isVisible: Boolean) =
            when(isVisible){
                true -> remind_button.visibility = View.VISIBLE
                false-> remind_button.visibility = View.INVISIBLE
            }

    override fun setVisibilityRemindProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> remindProgressBar.visibility = View.VISIBLE
                false-> remindProgressBar.visibility = View.INVISIBLE
            }

    override fun showToast(msg: String) =
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    override fun setEmailWrapperError(msg: String) {
        email_wrapper.error = msg
    }

    override fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun initPresenter(){
        presenter = RemindPasswordViewPresenter(NetworkProvider(applicationContext),
                FirebaseAuthProvider(this), this)
    }

    private fun initListener(){
        remind_button.setOnClickListener {
            presenter.onRemindButtonTap(email_edittext.text.toString()) }
    }
}