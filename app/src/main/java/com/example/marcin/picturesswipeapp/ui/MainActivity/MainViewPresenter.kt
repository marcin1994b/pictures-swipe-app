package com.example.marcin.picturesswipeapp.ui.MainActivity

import com.example.marcin.picturesswipeapp.Model.Objects.AppConfig
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.Model.Providers.SharedPreferencesProvider

class MainViewPresenter(private val networkProvider: NetworkProvider,
                        private val firebaseAuthProvider: FirebaseAuthProvider,
                        private val sharedPreferencesProvider: SharedPreferencesProvider,
                        private val view: MainViewContracts.MainView):
MainViewContracts.MainViewPresenter, FirebaseAuthProvider.OnFirebaseProvider{
//
    override fun onViewCreated() {
        setConfiguration()
        when(networkProvider.isNetworkAvailable()){
            true -> {
                firebaseAuthProvider.presenter = this
                when(firebaseAuthProvider.isUserSignedIn()){
                    true -> {
                        view.startStackView()
                        showEmailVerifyMsg()
                        firebaseAuthProvider.getCurrentUserEmail()?.let{
                            view.setNavDrawerTextView(it)
                        }
                    }
                    false -> view.startSignInView()
                }
            }
            false -> view.showToast(Strings.THERE_IS_NO_INTERNET_CONNECTION)
        }
    }

    override fun onNavStackViewItemTap() {
        view.closeNavDrawer()
        view.startStackView()
    }

    override fun onNavLikedImagesViewItemTap() {
        view.closeNavDrawer()
        view.startLikedImagesView()
    }

    override fun onNavAccountViewItemTap() {
        view.closeNavDrawer()
        view.startAccountView()
    }

    override fun onNavSettingsViewItemTap() {
        view.closeNavDrawer()
        view.startSettingsView()
    }

    override fun onNavSignOutItemTap() {
        view.closeNavDrawer()
        firebaseAuthProvider.signOut()
    }

    override fun onFirebaseProviderResult(isSuccess: Boolean, action: Int, msg: String) {
        if(action == firebaseAuthProvider.SIGN_OUT_USER_ACTION){
            when(isSuccess){
                true -> {
                    view.showToast("sign out completed!")
                    view.startSignInView()
                }
                false -> view.showToast(msg)
            }
        }
    }

    private fun showEmailVerifyMsg(){
        if(!firebaseAuthProvider.isUserEmailVerified()){
            view.showToast(Strings.VERIFY_YOUR_EMAIL)
        }
    }

    private fun setConfiguration(){
        setStackImageQualityConfig(sharedPreferencesProvider.
                restoreData(sharedPreferencesProvider.IMAGES_STACK_QUALITY_CONFIG_NAME))
        setFullImageQualityConfig(sharedPreferencesProvider.
                restoreData((sharedPreferencesProvider.FULL_IMAGES_QUALITY_CONFIG_NAME)))
        setMaxStackSizeConfig(sharedPreferencesProvider.
                restoreData(sharedPreferencesProvider.MAX_STACK_SIZE_CONFIG_NAME))
        setMinStackSizeConfig(sharedPreferencesProvider.
                restoreData(sharedPreferencesProvider.MIN_STACK_SIZE_CONFIG_NAME))
    }

    private fun setStackImageQualityConfig(value: Int){
        when(value){
            sharedPreferencesProvider.DEFAULT_VALUE -> {
                AppConfig.stackImagesQuality = AppConfig.LARGE_IMAGES_QUALITY
                sharedPreferencesProvider.
                        saveData(sharedPreferencesProvider.IMAGES_STACK_QUALITY_CONFIG_NAME,
                                AppConfig.stackImagesQuality)
            }
            else -> AppConfig.stackImagesQuality = value
        }
    }

    private fun setFullImageQualityConfig(value: Int){
        when(value){
            sharedPreferencesProvider.DEFAULT_VALUE -> {
                AppConfig.fullImagesQuality = AppConfig.LARGE_IMAGES_QUALITY
                sharedPreferencesProvider.
                        saveData(sharedPreferencesProvider.FULL_IMAGES_QUALITY_CONFIG_NAME,
                                AppConfig.fullImagesQuality)
            }
            else -> AppConfig.fullImagesQuality = value
        }
    }

    private fun setMaxStackSizeConfig(value: Int){
        when(value){
            sharedPreferencesProvider.DEFAULT_VALUE -> {
                AppConfig.maxStackSize = AppConfig.MEDIUM_MAX_STACK_SIZE
                sharedPreferencesProvider.
                        saveData(sharedPreferencesProvider.MAX_STACK_SIZE_CONFIG_NAME,
                                AppConfig.maxStackSize)
            }
            else -> AppConfig.maxStackSize = value
        }
    }

    private fun setMinStackSizeConfig(value: Int){
        when(value){
            sharedPreferencesProvider.DEFAULT_VALUE -> {
                AppConfig.minStackSize = AppConfig.MEDIUM_MIN_STACK_SIZE
                sharedPreferencesProvider.
                        saveData(sharedPreferencesProvider.MIN_STACK_SIZE_CONFIG_NAME,
                                AppConfig.minStackSize)
            }
            else -> AppConfig.minStackSize = value
        }
    }


}