package com.example.marcin.picturesswipeapp.Model.Objects

object Strings {

    val EMAIL_NOT_CONTAINS = "email do not contains @"
    val EMAIL_SEQUENCE_BEFORE_TOO_SHORT = "char sequence before @ is too short"
    val EMAIL_SEQUENCE_BEHIND_TOO_SHORT = "char sequence behind @ is too short"
    val EMAIL_NOT_CONTAINS_DOT = "must contains dot behind @"
    val EMAIL_TOO_SHORT_BEHIND_DOT = "char sequence behind dot is too short"

    val EMAIL_GOOD = ""

    val PASSWORD_ARE_DIFFERENT = "passwords are different"
    val PASSWORD_TOO_SHORT = "password too short"
    val PASSWORD_MUST_HAVE_LETTER = "password must contains a letter"
    val PASSWORD_MUST_HAVE_DIGIT = "password must contains a number"

    val PASSWORD_GOOD = ""

    val THERE_IS_NO_INTERNET_CONNECTION = "There is no Internet connection"

    val SHARE_PIC_STRING = "Look at this lovely pic, witch I found by app.\n"
    val INFO_VIEW_TITLE = "Photo"
    val HTTP_UNSPLASH_LINK = "https://unsplash.com/photos/..."

    val EMPTY_STRING = ""

    var SMALL = "Small"
    val MEDIUM = "Medium"
    val LARGE = "Large"

    //

    val I_HAVE_ALREADY_ACCOUNT = "I have already account."
    val I_DO_NOT_REMEMBER_PASSWORD = "I do not remember my password."
    val CREATE_NEW_ACCOUNT = "Create new account."

    val INPUT_EMAIL_AND_PASSWORD = "Input email and password, please"

    val WE_SEND_YOUR_PASSWORD = "We send password to your email address."
    val CONFIRM_YOUR_ADDRESS = "Confirm your address email"

    val VERIFY_YOUR_EMAIL = "Verify your email address, please."
}