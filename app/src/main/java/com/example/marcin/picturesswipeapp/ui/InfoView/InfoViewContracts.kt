package com.example.marcin.picturesswipeapp.ui.InfoView

interface InfoViewContracts {

    interface InfoView {

        fun setImageView(infoImgPath: String, fullImgPath: String)
        fun setAuthorProfileImageView(imgPath: String)
        fun setAuthorNameTextView(authorName: String)
        fun setUserNameTextView(userName: String)
        fun setDateTextView(date: String)
        fun setSizeTextView(size: String)
        fun setLinkTestView(link: String)
        fun startWebsite(url: String)
        fun startShareIntent(str: String)
        fun transitionToInfoViewMode(imgPath: String)
        fun transitionToImageViewMode(imgPath: String)
        fun setUpArrow()
        fun setDownArrow()
        fun setLeftArrow()
        fun setRightArrow()
        fun hideToolbar()
        fun showToolbar()
        fun showToast(msg: String)
        fun finishActivity()
    }

    interface InfoViewPresenter {

        fun onViewCreated(imgPositionOnStack: Int, isFromApi: Boolean)
        fun onUserNameTap()
        fun onUserProfileImgTap()
        fun onLinkTap()
        fun onArrowViewTap(viewOrientation: Int)
        fun onDownloadMenuButtonTap()
        fun onShareMenuButtonTap()
        fun onBackArrowMenuButtonTap()
    }
}