package com.example.marcin.picturesswipeapp.Model.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData
import com.example.marcin.picturesswipeapp.Model.Providers.PicassoProvider
import com.example.marcin.picturesswipeapp.R
import kotlinx.android.synthetic.main.image_card_item.view.*


class ImagesListAdapter(val imagesList: MutableList<ImageData>,
                        private val itemClick: (Int) -> Unit):
RecyclerView.Adapter<ImagesListAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindImage(imagesList[position], position)
    }

    override fun getItemCount(): Int = imagesList.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.image_card_item,
                parent, false)
        return ViewHolder(view, itemClick)
    }

    class ViewHolder(view: View, private val itemClick: (Int) -> Unit)
        : RecyclerView.ViewHolder(view){

        fun bindImage(imageData: ImageData, position: Int){
            val picassoProvider = PicassoProvider(itemView.context)
            with(imageData){
                picassoProvider.setImgViewInInfoActivity(urls.regular, itemView.imageView)
                itemView.setOnClickListener { itemClick(position) }
            }
        }

    }
}