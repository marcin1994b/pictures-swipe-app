package com.example.marcin.picturesswipeapp.ui.InfoView

import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.transition.TransitionManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.Objects.Consts
import com.example.marcin.picturesswipeapp.Model.Objects.Strings
import com.example.marcin.picturesswipeapp.Model.Providers.*
import com.example.marcin.picturesswipeapp.R
import kotlinx.android.synthetic.main.activity_info.*


class InfoActivity : AppCompatActivity(), InfoViewContracts.InfoView {

    lateinit var presenter : InfoViewContracts.InfoViewPresenter
    lateinit var picassoProvider : PicassoProvider
    private lateinit var infoViewSet : ConstraintSet
    private lateinit var imageViewSet : ConstraintSet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        inits()
        presenter.onViewCreated(intent.getIntExtra("imgPosition", -1),
                intent.getBooleanExtra("isFromApi", false))
    }

    override fun finishActivity() {
        finish()
    }

    override fun showToast(msg: String) {
        Toast.makeText(this.applicationContext, msg, Toast.LENGTH_SHORT).show()
    }

    override fun setImageView(infoImgPath: String, fullImgPath: String) {
        picassoProvider.setImgViewInInfoActivity(infoImgPath, imgView)
        picassoProvider.setImgFullView(fullImgPath, imgViewFull)
    }

    override fun setAuthorProfileImageView(imgPath: String) {
        picassoProvider.setImgViewInInfoActivity(imgPath, profilImageView)
    }

    override fun setAuthorNameTextView(authorName: String) {
        authorNameTextView.text = authorName
    }

    override fun setUserNameTextView(userName: String) {
        val content = SpannableString(userName)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        userNameTextView.text = content
    }

    override fun setDateTextView(date: String) {
        dateTextView.text = date
    }

    override fun setSizeTextView(size: String) {
        sizeTextView.text = size
    }

    override fun setLinkTestView(link: String) {
        val content = SpannableString(link)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        linkTextView.text = content
    }

    override fun startWebsite(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    override fun startShareIntent(str: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT, Strings.SHARE_PIC_STRING + str)
        intent.type = Consts.INTENT_ACTION_TYPE
        startActivity(intent)
    }

    override fun setUpArrow() {
        arrowView.background = getDrawable(R.drawable.uparrow)
    }

    override fun setDownArrow() {
        arrowView.background = getDrawable(R.drawable.downarrow)
    }

    override fun setLeftArrow() {
        arrowView.background = getDrawable(R.drawable.leftarrow)
    }

    override fun setRightArrow() {
        arrowView.background = getDrawable(R.drawable.rightarrow)
    }

    override fun hideToolbar() {
        toolbar.visibility = View.GONE
    }

    override fun showToolbar() {
        toolbar.visibility = View.VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.info_view_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.menu_info_view_download -> presenter.onDownloadMenuButtonTap()
            R.id.menu_info_view_share -> presenter.onShareMenuButtonTap()
            android.R.id.home -> presenter.onBackArrowMenuButtonTap()
        }
        return true
    }

    override fun transitionToInfoViewMode(imgPath: String) {
        TransitionManager.beginDelayedTransition(activity_info_layout)
        infoViewSet.applyTo(activity_info_layout)
    }

    override fun transitionToImageViewMode(imgPath: String) {
        TransitionManager.beginDelayedTransition(activity_info_layout)
        imageViewSet.applyTo(activity_info_layout)
    }

    private fun inits(){
        initPicassoProvider()
        initToolbar()
        initPresenter()
        initListeners()
        initConstraintSets()
    }

    private fun initPicassoProvider(){
        picassoProvider = PicassoProvider(applicationContext)
    }

    private fun initToolbar(){
        toolbar.title = Strings.INFO_VIEW_TITLE
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initPresenter(){
        presenter = InfoViewPresenter(this, NetworkProvider(applicationContext),
                PermissionsProvider(this, applicationContext),
                DownloadFileProvider(applicationContext),
                PackageManagerProvider(applicationContext))
    }

    private fun initListeners(){
        profilImageView.setOnClickListener {presenter.onUserProfileImgTap()}
        userNameTextView.setOnClickListener {presenter.onUserNameTap()}
        linkTextView.setOnClickListener {presenter.onLinkTap()}
        arrowView.setOnClickListener {
            if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                presenter.onArrowViewTap(Consts.ORIENTATION_LANDSCAPE)
            }else if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
                presenter.onArrowViewTap(Consts.ORIENTATION_PORTRAIT)
            }}
    }


    private fun initConstraintSets(){
        infoViewSet = ConstraintSet()
        infoViewSet.clone(activity_info_layout)

        imageViewSet = ConstraintSet()
        imageViewSet.clone(this, R.layout.activity_info_img)
    }
}
