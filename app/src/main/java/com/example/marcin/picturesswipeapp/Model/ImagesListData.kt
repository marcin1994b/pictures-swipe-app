package com.example.marcin.picturesswipeapp.Model

import com.example.marcin.picturesswipeapp.Model.ImageData.ImageData

class ImagesListData(){

    constructor(imagesList : HashMap<String, ImageData>) : this(){
        this.imagesList = imagesList
    }

    var imagesList = HashMap<String, ImageData>()

    fun hashMapToList(): MutableList<ImageData>{
        val list = mutableListOf<ImageData>()
        imagesList.forEach { entry -> list.add(entry.value) }
        return list
    }
}