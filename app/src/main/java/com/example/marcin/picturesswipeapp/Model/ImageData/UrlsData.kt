package com.example.marcin.picturesswipeapp.Model.ImageData

class UrlsData(){

    constructor(raw: String, full: String, regular: String, small: String, thumb: String): this() {
        this.raw = raw
        this.full = full
        this.regular = regular
        this.small = small
        this.thumb = thumb
    }

    var raw: String = ""
    var full: String = ""
    var regular: String = ""
    var small: String = ""
    var thumb: String = ""
}