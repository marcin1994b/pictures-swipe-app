package com.example.marcin.picturesswipeapp.ui.AccountView

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.transition.AutoTransition
import android.support.transition.TransitionManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.marcin.picturesswipeapp.Model.InputValidator
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseAuthProvider
import com.example.marcin.picturesswipeapp.Model.Providers.FirebaseDatabaseProvider
import com.example.marcin.picturesswipeapp.Model.Providers.NetworkProvider
import com.example.marcin.picturesswipeapp.R
import com.example.marcin.picturesswipeapp.ui.LoginView.LoginActivity
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.change_email_layout.view.*
import kotlinx.android.synthetic.main.change_password_layout.view.*
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.restart_account_layout.view.*
import kotlinx.android.synthetic.main.verify_email_layout.view.*



class AccountFragment: Fragment(), AccountViewContracts.AccountView {

    var presenter: AccountViewContracts.AccountViewPresenter? = null
    private lateinit var layout : View

    companion object {
        fun newInstance(): Fragment = AccountFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater!!.inflate(R.layout.fragment_account, container, false)
        initPresenter()
        setArrowsListeners()
        setButtonsListener()
        initObservables()
        return layout
    }

    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun expandVerifyView() {
        beginDelayedTransition()
        layout.verifyEmailView.let{
            it.noteTextView.visibility = View.VISIBLE
            it.verifyButton.visibility = View.VISIBLE
            it.verifyEmailProgressBar.visibility = View.INVISIBLE
            setCollapseArrow(it.verifyArrowImageView)
        }
    }

    override fun expandChangeEmailView() {
        beginDelayedTransition()
        layout.changeEmailView.let{
            it.saveEmailButton.visibility = View.VISIBLE
            it.passwordInputWrapper.visibility = View.VISIBLE
            it.newEmailInputWrapper.visibility = View.VISIBLE
            it.changeEmailProgressBar.visibility = View.INVISIBLE
            setCollapseArrow(it.changeEmailArrowImageView)
        }
    }

    override fun expandChangePasswordView() {
        beginDelayedTransition()
        layout.changePasswordView.let{
            it.currentPasswordWrapper.visibility = View.VISIBLE
            it.firstNewPasswordWrapper.visibility = View.VISIBLE
            it.secondNewPasswordWrapper.visibility = View.VISIBLE
            it.savePasswordChangeButton.visibility = View.VISIBLE
            it.changePasswordProgressBar.visibility = View.INVISIBLE
            setCollapseArrow(it.changePasswordArrowImageView)
        }
    }

    override fun expandRestartAccountView() {
        beginDelayedTransition()
        layout.restartAccountView.let{
            it.messageTextView.visibility = View.VISIBLE
            it.passwordWrapper.visibility = View.VISIBLE
            it.submitButton.visibility = View.VISIBLE
            it.restartAccountProgressBar.visibility = View.INVISIBLE
            setCollapseArrow(it.arrowImageView)
        }
    }

    override fun expandRemoveAccountView() {
        beginDelayedTransition()
        layout.removeAccountView.let{
            it.messageTextView.visibility = View.VISIBLE
            it.passwordWrapper.visibility = View.VISIBLE
            it.submitButton.visibility = View.VISIBLE
            it.restartAccountProgressBar.visibility = View.INVISIBLE
            setCollapseArrow(it.arrowImageView)
        }
    }

    override fun collapseVerifyView() {
        beginDelayedTransition()
        layout.verifyEmailView.let{
            it.noteTextView.visibility = View.GONE
            it.verifyButton.visibility = View.GONE
            it.verifyEmailProgressBar.visibility = View.GONE
            setExpandArrow(it.verifyArrowImageView)
        }
    }

    override fun collapseChangeEmailView() {
        beginDelayedTransition()
        layout.changeEmailView.let{
            it.saveEmailButton.visibility = View.GONE
            it.passwordInputWrapper.visibility = View.GONE
            it.newEmailInputWrapper.visibility = View.GONE
            it.changeEmailProgressBar.visibility = View.GONE
            setExpandArrow(it.changeEmailArrowImageView)
        }
    }

    override fun collapseChangePasswordView() {
        beginDelayedTransition()
        layout.changePasswordView.let{
            it.currentPasswordWrapper.visibility = View.GONE
            it.firstNewPasswordWrapper.visibility = View.GONE
            it.secondNewPasswordWrapper.visibility = View.GONE
            it.savePasswordChangeButton.visibility = View.GONE
            it.changePasswordProgressBar.visibility = View.GONE
            setExpandArrow(it.changePasswordArrowImageView)
        }
    }

    override fun collapseRestartAccountView() {
        beginDelayedTransition()
        layout.restartAccountView.let{
            it.messageTextView.visibility = View.GONE
            it.passwordWrapper.visibility = View.GONE
            it.submitButton.visibility = View.GONE
            it.restartAccountProgressBar.visibility = View.GONE
            setExpandArrow(it.arrowImageView)
        }
    }

    override fun collapseRemoveAccountView() {
        beginDelayedTransition()
        layout.removeAccountView.let{
            it.messageTextView.visibility = View.GONE
            it.passwordWrapper.visibility = View.GONE
            it.submitButton.visibility = View.GONE
            it.restartAccountProgressBar.visibility = View.GONE
            setExpandArrow(it.arrowImageView)
        }
    }

    override fun setEnabledVerifyEmailButton(isEnable: Boolean) {
        layout.verifyEmailView.verifyButton.isEnabled = isEnable
    }

    override fun setEnabledChangeEmailButton(isEnable: Boolean) {
        layout.changeEmailView.saveEmailButton.isEnabled = isEnable
    }

    override fun setEnabledChangePasswordButton(isEnable: Boolean){
        layout.changePasswordView.savePasswordChangeButton.isEnabled = isEnable
    }

    override fun setEnabledRestartAccountButton(isEnable: Boolean){
        layout.restartAccountView.submitButton.isEnabled = isEnable
    }

    override fun setEnabledRemoveAccountButton(isEnable: Boolean){
        layout.removeAccountView.submitButton.isEnabled = isEnable
    }

    override fun setVisibilityVerifyButton(isVisible: Boolean) =
            when(isVisible){
                true -> layout.verifyEmailView.verifyButton.visibility = View.VISIBLE
                false-> layout.verifyEmailView.verifyButton.visibility = View.INVISIBLE
            }

    override fun setVisibilityChangeEmailButton(isVisible: Boolean) =
            when(isVisible){
                true -> layout.changeEmailView.saveEmailButton.visibility = View.VISIBLE
                false-> layout.changeEmailView.saveEmailButton.visibility = View.INVISIBLE
            }

    override fun setVisibilityChangePasswordButton(isVisible: Boolean) =
            when(isVisible){
                true -> layout.changePasswordView.savePasswordChangeButton.visibility = View.VISIBLE
                false-> layout.changePasswordView.savePasswordChangeButton.visibility = View.INVISIBLE
            }

    override fun setVisibilityRestartAccountButton(isVisible: Boolean) =
            when(isVisible){
                true -> layout.restartAccountView.submitButton.visibility = View.VISIBLE
                false-> layout.restartAccountView.submitButton.visibility = View.INVISIBLE
            }

    override fun setVisibilityRemoveAccountButton(isVisible: Boolean) =
            when(isVisible){
                true -> layout.removeAccountView.submitButton.visibility = View.VISIBLE
                false-> layout.removeAccountView.submitButton.visibility = View.INVISIBLE
            }

    override fun setVisibilityVerifyProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> layout.verifyEmailView.verifyEmailProgressBar.visibility = View.VISIBLE
                false-> layout.verifyEmailView.verifyEmailProgressBar.visibility = View.INVISIBLE
            }

    override fun setVisibilityChangeEmailProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> layout.changeEmailView.changeEmailProgressBar.visibility = View.VISIBLE
                false-> layout.changeEmailView.changeEmailProgressBar.visibility = View.INVISIBLE
            }

    override fun setVisibilityChangePasswordProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> layout.changePasswordView.changePasswordProgressBar.visibility = View.VISIBLE
                false-> layout.changePasswordView.changePasswordProgressBar.visibility = View.INVISIBLE
            }
    override fun setVisibilityRestartAccountProgressBar(isVisible: Boolean) =
            when(isVisible){
                true -> layout.restartAccountView.restartAccountProgressBar.visibility = View.VISIBLE
                false-> layout.restartAccountView.restartAccountProgressBar.visibility = View.INVISIBLE
            }
    override fun setVisibilityRemoveAccountProgressBar(isVisible: Boolean) =
            when(isVisible){
            true -> layout.removeAccountView.restartAccountProgressBar.visibility = View.VISIBLE
                false-> layout.removeAccountView.restartAccountProgressBar.visibility = View.INVISIBLE
            }

    override fun setVerifyView(isEmailVerified: Boolean) {
        when(isEmailVerified){
            true -> {
                setEmailVerifiedTextNote()
                setEnabledVerifyEmailButton(false)
            }
            false -> setEmailNotVerifiedTextNote()
        }
    }

    override fun setRemoveAccountView() {
        layout.removeAccountView.let{
            it.headTextView.text = "Remove account"
            it.submitButton.text = "REMOVE"
        }
    }

    override fun setNewEmailWrapperErrorInChangeEmailView(error: String) {
        layout.changeEmailView.newEmailInputWrapper.error = error
    }

    override fun setFirstNewPassWrapperErrorInChangePasswordView(error: String) {
        layout.changePasswordView.firstNewPasswordWrapper.error = error
    }

    override fun setSecondNewPassWrapperErrorInChangePasswordView(error: String) {
        layout.changePasswordView.secondNewPasswordWrapper.error = error
    }

    override fun startSignInActivity() {
        val intent = Intent(this.context, LoginActivity::class.java)
        startActivity(intent)
        this.activity.finish()
    }

    private fun initPresenter(){
        if(presenter == null) {
            presenter = AccountViewPresenter(this, InputValidator(),
                    FirebaseAuthProvider(this.activity), FirebaseDatabaseProvider(), NetworkProvider(this.context))
        }
    }

    private fun initObservables(){
        presenter?.onViewCreated()
        presenter?.observChangeEmailView(
                layout.changeEmailView.emailEditText.textChanges(),
                layout.changeEmailView.passwordEditTextInChangeEmail.textChanges()
        )
        presenter?.observChangePasswordView(
                layout.changePasswordView.currentPasswordEditText.textChanges(),
                layout.changePasswordView.firstNewPasswordEditText.textChanges(),
                layout.changePasswordView.secondNewPasswordEditText.textChanges()
        )
        presenter?.observRestartAccountView(layout.restartAccountView.passwordEditText.textChanges())
        presenter?.observRemoveAccountView(layout.removeAccountView.passwordEditText.textChanges())
    }

    private fun beginDelayedTransition(){
        TransitionManager.beginDelayedTransition(layout as ViewGroup, AutoTransition())
    }

    private fun setExpandArrow(view: View){
        view.background = resources.getDrawable(R.drawable.ic_expand_arrow)
    }

    private fun setCollapseArrow(view: View){
        view.background = resources.getDrawable(R.drawable.ic_collapse_arrow)
    }

    private fun setEmailVerifiedTextNote(){
        layout.verifyEmailView.let{
            it.noteTextView.setTextColor(Color.GREEN)
            it.noteTextView.text = "Lorem ipsum dolor sit amet."
        }
    }

    private fun setEmailNotVerifiedTextNote(){
        layout.verifyEmailView.let{
            it.noteTextView.setTextColor(Color.RED)
            it.noteTextView.text = "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis."
        }
    }

    private fun setArrowsListeners(){
        layout.verifyEmailView.verifyArrowImageView.setOnClickListener {
            presenter?.onVerifyViewArrowTap()
        }
        layout.changeEmailView.changeEmailArrowImageView.setOnClickListener {
            presenter?.onChangeEmailViewArrowTap()
        }
        layout.changePasswordView.changePasswordArrowImageView.setOnClickListener {
            presenter?.onChangePasswordViewArrowTap()
        }
        layout.restartAccountView.arrowImageView.setOnClickListener {
            presenter?.onRestartAccountViewArrowTap()
        }
        layout.removeAccountView.arrowImageView.setOnClickListener {
            presenter?.onRemoveAccountViewArrowTap()
        }
    }

    private fun setButtonsListener(){
        layout.verifyEmailView.verifyButton.setOnClickListener { presenter?.onVerifyViewButtonTap() }
        layout.changeEmailView.saveEmailButton.setOnClickListener {
            presenter?.onChangeEmailViewButtonTap(
                    layout.changeEmailView.emailEditText.text.toString(),
                    layout.changeEmailView.passwordEditTextInChangeEmail.text.toString())
        }
        layout.changePasswordView.savePasswordChangeButton.setOnClickListener {
            presenter?.onChangePasswordViewButtonTap(
                    layout.changePasswordView.currentPasswordEditText.text.toString(),
                    layout.changePasswordView.firstNewPasswordEditText.text.toString()
            )
        }
        layout.restartAccountView.submitButton.setOnClickListener {
            presenter?.onRestartAccountViewButtonTap(
                    layout.restartAccountView.passwordEditText.text.toString()
            )
        }
        layout.removeAccountView.submitButton.setOnClickListener {
            presenter?.onRemoveAccountViewButtonTap(
                    layout.removeAccountView.passwordEditText.text.toString()
            )
        }
    }

}